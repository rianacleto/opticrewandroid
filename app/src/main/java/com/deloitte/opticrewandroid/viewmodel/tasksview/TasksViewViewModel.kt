package com.deloitte.opticrewandroid.viewmodel.tasksview

import android.app.Application
import android.os.Parcelable
import androidx.lifecycle.*
import com.deloitte.opticrewandroid.MockProvider
import com.deloitte.opticrewandroid.usecases.EmployeeToOperationsMergeUseCase
import com.deloitte.opticrewandroid.usecases.EmployeeToOperationsMergeUseCase.ProcessedDataResult
import com.deloitte.opticrewandroid.viewmodel.EntityViewModelFactory
import com.deloitte.opticrewandroid.viewmodel.myworkcenterstype.MyWorkCentersTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.operationstype.OperationsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.workcenteremployeestype.WorkCenterEmployeesTypeViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.*
import kotlinx.coroutines.flow.MutableStateFlow

class TasksViewViewModel(
    val lifecycleOwner: LifecycleOwner,
    val application: Application,
    private val viewModelStoreOwner: ViewModelStoreOwner,
    private val employeeToOperationsUseCase: EmployeeToOperationsMergeUseCase,
) : ViewModel() {

    //ViewModels
    private lateinit var workCentersViewModel: MyWorkCentersTypeViewModel
    private lateinit var employeesViewModel: WorkCenterEmployeesTypeViewModel
    private lateinit var operationsTypeViewModel: OperationsTypeViewModel

    // Data to process
    var workCenters = MutableLiveData<List<MyWorkCentersType>>(emptyList())
    private var operations: List<OperationsType> = emptyList()
    private var employees: List<WorkCenterEmployeesType> = emptyList()

    //Controll the data load status in order to process it
    private var toOperationLoaded = false
    private var toEmployeesLoaded = false

    //Control if there is an error present and show it
    var errorMsg = MutableLiveData<String>()

    // control the vibilility of
    var isLoading = MutableLiveData(false)

    // grid number of columns
    var columnCount = MutableLiveData<Int?>(null)

    // gridItems list
    var employeeAndOperationProcessingResult = MutableLiveData<ProcessedDataResult>()

    fun getWorkCenters() {
        workCentersViewModel =
            ViewModelProvider(viewModelStoreOwner).get(MyWorkCentersTypeViewModel::class.java)
                .also {
                    it.initialRead { errorMessage ->
                        errorMsg.value = errorMessage
                    }
                }
        workCentersViewModel.observableItems.observe(
            lifecycleOwner,
            { items ->
                workCenters.value = items
                //onWorkCenterSelected(items[0])
            }
        )

        workCentersViewModel.readResult.observe(lifecycleOwner, {
            isLoading.value = false
        })

    }

    private fun prepareEmployeesViewModelAndGetEmmployees(
        navigationPropertyName: String = "to_WorkCenterEmployees",
        parentEntity: Parcelable?
    ) {
        employeesViewModel = if (parentEntity != null) {
            ViewModelProvider(
                viewModelStoreOwner,
                EntityViewModelFactory(application, navigationPropertyName!!, parentEntity!!)
            )
                .get(WorkCenterEmployeesTypeViewModel::class.java)
        } else {
            ViewModelProvider(viewModelStoreOwner).get(WorkCenterEmployeesTypeViewModel::class.java)
                .also {
                    employeesViewModel = it
                    it.initialRead { errorMessage ->
                        errorMsg.value = errorMessage
                    }
                }
        }
        employeesViewModel.observableItems.observe(lifecycleOwner, { items ->
            employees = items
        })

        employeesViewModel.readResult.observe(lifecycleOwner, {
            if (employees.isNotEmpty()) {
                toEmployeesLoaded = true
            }
            if (toOperationLoaded && toEmployeesLoaded) {
                processData()
            }
        })
    }

    private fun prepareOperationsViewModelAndGetOperations(
        navigationPropertyName: String = "to_Operations",
        parentEntityData: Parcelable?
    ) {
        operationsTypeViewModel = if (parentEntityData != null) {
            ViewModelProvider(
                viewModelStoreOwner,
                EntityViewModelFactory(
                    application,
                    navigationPropertyName!!,
                    parentEntityData!!
                )
            )
                .get(OperationsTypeViewModel::class.java)
        } else {
            ViewModelProvider(viewModelStoreOwner).get(OperationsTypeViewModel::class.java).also {
                it.initialRead { errorMessage ->
                    errorMsg.value = errorMessage
                }
            }
        }
        operationsTypeViewModel.observableItems.observe(
            lifecycleOwner,
            { items ->
                //operations = items
                operations = MockProvider.getMockOperations()
            })

        operationsTypeViewModel.readResult.observe(lifecycleOwner, {
            if (operations.isNotEmpty()) {
                toOperationLoaded = true
            }
            if (toOperationLoaded && toEmployeesLoaded) {
                processData()
            }
            isLoading.value = false
        })
    }

    private fun processData() {
        val result: ProcessedDataResult =
            employeeToOperationsUseCase.processLoadedData(operations, employees)
        employeeAndOperationProcessingResult.value = result
        columnCount.value = result.spanCount
    }

    fun onWorkCenterSelected(position: Int) {
        prepareOperationsViewModelAndGetOperations(parentEntityData = workCenters.value?.get(position))
        prepareEmployeesViewModelAndGetEmmployees(parentEntity = workCenters.value?.get(position))
    }

    class Factory(
        private val lifecycleOwner: LifecycleOwner,
        private val application: Application,
        private val viewModelStoreOwner: ViewModelStoreOwner,
        private val employeeToOperationsUseCase: EmployeeToOperationsMergeUseCase,
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(TasksViewViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return TasksViewViewModel(
                    lifecycleOwner,
                    application,
                    viewModelStoreOwner,
                    employeeToOperationsUseCase
                ) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }

}