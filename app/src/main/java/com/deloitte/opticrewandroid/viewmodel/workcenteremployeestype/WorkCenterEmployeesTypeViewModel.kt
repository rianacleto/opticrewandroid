package com.deloitte.opticrewandroid.viewmodel.workcenteremployeestype

import android.app.Application
import android.os.Parcelable

import com.deloitte.opticrewandroid.viewmodel.EntityViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets

/*
 * Represents View model for WorkCenterEmployeesType
 *
 * Having an entity view model for each <T> allows the ViewModelProvider to cache and return the view model of that
 * type. This is because the ViewModelStore of ViewModelProvider cannot not be able to tell the difference between
 * EntityViewModel<type1> and EntityViewModel<type2>.
 */
class WorkCenterEmployeesTypeViewModel(application: Application): EntityViewModel<WorkCenterEmployeesType>(application, EntitySets.workCenterEmployees, WorkCenterEmployeesType.personName) {
    /**
     * Constructor for a specific view model with navigation data.
     * @param [navigationPropertyName] - name of the navigation property
     * @param [entityData] - parent entity (starting point of the navigation)
     */
    constructor(application: Application, navigationPropertyName: String, entityData: Parcelable): this(application) {
        EntityViewModel<WorkCenterEmployeesType>(application, EntitySets.workCenterEmployees, WorkCenterEmployeesType.personName, navigationPropertyName, entityData)
    }
}
