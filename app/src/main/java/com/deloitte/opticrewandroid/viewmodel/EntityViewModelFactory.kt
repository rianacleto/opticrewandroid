package com.deloitte.opticrewandroid.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import android.os.Parcelable

import com.deloitte.opticrewandroid.viewmodel.employeestype.EmployeesTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.materialstype.MaterialsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.myequipmentstype.MyEquipmentsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.myfunctionallocationstype.MyFunctionalLocationsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.myworkcenterstype.MyWorkCentersTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.myworkorderheaderstype.MyWorkOrderHeadersTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.myworkorderoperationstype.MyWorkOrderOperationsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.operationcomponentstype.OperationComponentsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.operationstype.OperationsTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.workcenteremployeestype.WorkCenterEmployeesTypeViewModel
import com.deloitte.opticrewandroid.viewmodel.workcenterstype.WorkCentersTypeViewModel

/**
 * Custom factory class, which can create view models for entity subsets, which are
 * reached from a parent entity through a navigation property.
 *
 * @param application parent application
 * @param navigationPropertyName name of the navigation link
 * @param entityData parent entity
 */
class EntityViewModelFactory (
        val application: Application, // name of the navigation property
        val navigationPropertyName: String, // parent entity
        val entityData: Parcelable) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (modelClass.simpleName) {
			"EmployeesTypeViewModel" -> EmployeesTypeViewModel(application, navigationPropertyName, entityData) as T
            			"MaterialsTypeViewModel" -> MaterialsTypeViewModel(application, navigationPropertyName, entityData) as T
            			"MyEquipmentsTypeViewModel" -> MyEquipmentsTypeViewModel(application, navigationPropertyName, entityData) as T
            			"MyFunctionalLocationsTypeViewModel" -> MyFunctionalLocationsTypeViewModel(application, navigationPropertyName, entityData) as T
            			"MyWorkCentersTypeViewModel" -> MyWorkCentersTypeViewModel(application, navigationPropertyName, entityData) as T
            			"MyWorkOrderHeadersTypeViewModel" -> MyWorkOrderHeadersTypeViewModel(application, navigationPropertyName, entityData) as T
            			"MyWorkOrderOperationsTypeViewModel" -> MyWorkOrderOperationsTypeViewModel(application, navigationPropertyName, entityData) as T
            			"OperationComponentsTypeViewModel" -> OperationComponentsTypeViewModel(application, navigationPropertyName, entityData) as T
            			"OperationsTypeViewModel" -> OperationsTypeViewModel(application, navigationPropertyName, entityData) as T
            			"WorkCenterEmployeesTypeViewModel" -> WorkCenterEmployeesTypeViewModel(application, navigationPropertyName, entityData) as T
             else -> WorkCentersTypeViewModel(application, navigationPropertyName, entityData) as T
        }
    }
}
