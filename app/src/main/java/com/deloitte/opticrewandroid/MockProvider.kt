package com.deloitte.opticrewandroid

import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import com.sap.cloud.mobile.odata.LocalTime

object MockProvider {
    fun getMockOperations(): List<OperationsType> {
        val operationsList = mutableListOf<OperationsType>()
        for (i in 1..8) {
            val operation = OperationsType()
            operation.startTime = LocalTime.of(9, 0, 0, 0)
            operation.endTime = LocalTime.of(10, 15, 0, 0)
            operation.personNumber = when (i) {
                1 -> "59"
                2 -> "777"//"63"
                3 -> "247"
                4 -> "243"
                5 -> "250"
                6 -> "272"
                7 -> "999"//"273"
                else -> ""
            }
            operationsList.add(operation)
        }

        var operation = OperationsType()
        operation.startTime = LocalTime.of(13, 0, 0, 0)
        operation.endTime = LocalTime.of(16, 0, 0, 0)
        operation.personNumber = "63"
        operationsList.add(operation)

        operation = OperationsType()
        operation.startTime = LocalTime.of(17, 23, 0, 0)
        operation.endTime = LocalTime.of(19, 19, 0, 0)
        operation.personNumber = "63"
        operationsList.add(operation)

        operation = OperationsType()
        operation.startTime = LocalTime.of(12, 12, 0, 0)
        operation.endTime = LocalTime.of(14, 14
            , 0, 0)
        operation.personNumber = "273"
        operationsList.add(operation)

        return operationsList
    }
}