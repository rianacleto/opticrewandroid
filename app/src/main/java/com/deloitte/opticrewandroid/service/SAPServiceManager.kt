package com.deloitte.opticrewandroid.service

import com.sap.cloud.mobile.foundation.model.AppConfig
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_Entities
import com.sap.cloud.mobile.foundation.common.ClientProvider
import com.sap.cloud.mobile.odata.OnlineODataProvider
import com.sap.cloud.mobile.odata.http.OKHttpHandler

class SAPServiceManager(private val appConfig: AppConfig) {

    var serviceRoot: String = ""
        private set
        get() {
            return (cds_zsd_sam_odata_Entities?.provider as OnlineODataProvider).serviceRoot
        }

    var cds_zsd_sam_odata_Entities: cds_zsd_sam_odata_Entities? = null
        private set
        get() {
            return field ?: throw IllegalStateException("SAPServiceManager was not initialized")
        }

    fun openODataStore(callback: () -> Unit) {
        if( appConfig != null ) {
            appConfig.serviceUrl?.let { _serviceURL ->
                cds_zsd_sam_odata_Entities = cds_zsd_sam_odata_Entities (
                    OnlineODataProvider("SAPService", _serviceURL + CONNECTION_ID_CDS_ZSD_SAM_ODATA_ENTITIES).apply {
                        networkOptions.httpHandler = OKHttpHandler(ClientProvider.get())
                        serviceOptions.checkVersion = false
                        serviceOptions.requiresType = true
                        serviceOptions.cacheMetadata = false
                    }
                )
            } ?: run {
                throw IllegalStateException("ServiceURL of Configuration Data is not initialized")
            }
        }
        callback.invoke()
    }

    companion object {
        const val CONNECTION_ID_CDS_ZSD_SAM_ODATA_ENTITIES: String = "com.deloitte.opticrew"
    }
}
