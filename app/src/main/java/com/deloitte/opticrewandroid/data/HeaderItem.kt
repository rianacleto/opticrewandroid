package com.deloitte.opticrewandroid.data

data class HeaderItem(
    val width: Int,
    val headerTime: String? = null
)
