package com.deloitte.opticrewandroid.data

import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType

data class EmployeeViewItem(
    var employeesType: WorkCenterEmployeesType?,
    val rowNum: Int
)
