package com.deloitte.opticrewandroid.data

import com.deloitte.opticrewandroid.adapters.OperationsViewType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType

data class TaskViewItem(
    var operationsType: OperationsType?,
    val rowNum: Int,
    val viewType: OperationsViewType,
    val width: Int,
    val headerTime: String? = null
)
