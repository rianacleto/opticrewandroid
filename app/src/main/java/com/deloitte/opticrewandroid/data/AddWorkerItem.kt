package com.deloitte.opticrewandroid.data

import com.deloitte.opticrewandroid.adapters.AddWorkersPopUpAdapter
import com.deloitte.opticrewandroid.adapters.AddWorkersPopUpViewType

data class AddWorkerItem(
    var isChecked:Boolean,
    val workerName: String,
    val workerNumber:String,
    val skill: String,
    val viewType: AddWorkersPopUpViewType,
    val status: AddWorkersPopUpAdapter.WorkerStatus,
    val title: String,
)
