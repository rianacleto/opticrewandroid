package com.deloitte.opticrewandroid

import android.content.Context

object Utils {
    fun pixelsFromDps(dps: Int, context: Context): Int {
        val scale: Float = context.resources.displayMetrics.density
        return (dps * scale + 0.5f).toInt()
    }
}