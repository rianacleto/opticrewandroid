package com.deloitte.opticrewandroid.usecases

import com.deloitte.opticrewandroid.adapters.OperationsViewType.*
import com.deloitte.opticrewandroid.configurations.Constants.HEADER_WIDTH
import com.deloitte.opticrewandroid.data.HeaderItem
import com.deloitte.opticrewandroid.data.TaskViewItem
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType
import com.sap.cloud.mobile.odata.LocalTime

class EmployeeToOperationsMergeUseCase(
) {
    fun processLoadedData(
        operations: List<OperationsType>,
        employees: List<WorkCenterEmployeesType>
    ): ProcessedDataResult {
        var rowNum = 0
        val spanCount: Int
        val employeeList = ArrayList<WorkCenterEmployeesType>()
        var operationsList = ArrayList<TaskViewItem>()
        var startTime = LocalTime.of(23, 59, 59)
        var endTime = LocalTime.of(0, 0, 0)
        employees.forEach { employee ->
            rowNum++
            var operationsAdded = false

            employeeList.add(employee)

            operations.filter { it.personNumber.equals(employee.personNumber) }
                .forEach { operation ->
                    var taskSpan = 1

                    operation.startTime?.also { opStartTime ->
                        operation.endTime?.also { opEndTime ->
                            // check the start time
                            if (opStartTime.lessThan(startTime)) {
                                startTime = opStartTime
                            }
                            // check the end time
                            if (opEndTime.greaterThan(endTime)) {
                                endTime = opEndTime
                            }
                            // set task span
                            taskSpan = (((opEndTime.hour - opStartTime.hour) * 60) -
                                    opStartTime.minute + opEndTime.minute)

                        }
                    }


                    // Add the task
                    operationsList.add(
                        TaskViewItem(
                            operation,
                            rowNum,
                            TASK,
                            taskSpan
                        )
                    )
                    operationsAdded = true
                }
            if (!operationsAdded) {
                operationsList.add(
                    TaskViewItem(
                        null,
                        rowNum,
                        EMPTY,
                        (endTime.hour + 1 - startTime.hour) * 60 - startTime.minute
                    )
                )
            }
        }

        spanCount = (endTime.hour + 1 - startTime.hour) * 60

        operationsList = addWhiteSpacesBetweenTasks(
            operationsList,
            startTime,
            endTime
        )

        return ProcessedDataResult(
            spanCount,
            employeeList,
            operationsList,
            setHeaderRow(
                spanCount,
                operationsList[0].operationsType?.startTime ?: LocalTime.now()
            )
        )
    }

    private fun addWhiteSpacesBetweenTasks(
        itemsList: ArrayList<TaskViewItem>,
        startTime: LocalTime,
        endTime: LocalTime
    ): ArrayList<TaskViewItem> {
        val returnList = ArrayList<TaskViewItem>()
        val first = itemsList.first()
        first.operationsType?.startTime?.also { firstOpStartTime ->
            if (firstOpStartTime.notEqual(startTime)) {
                returnList.add(getInitialWhiteSpace(first.rowNum, firstOpStartTime, startTime))
            }
        }

        for (i in 1 until itemsList.size) {

            val item = itemsList[i]
            val previousItem = itemsList[(i - 1)]

            returnList.add(previousItem)

            val itemToAdd = processWhiteSpaceLogic(previousItem, item, endTime)
            itemToAdd?.also {
                returnList.add(it)
            }
            if ((item.rowNum != previousItem.rowNum) &&
                startTime != item.operationsType?.startTime
            ) {
                item.operationsType?.startTime?.also { lastStartTime ->
                    returnList.add(
                        getInitialWhiteSpace(
                            previousItem.rowNum,
                            lastStartTime,
                            startTime
                        )
                    )
                }
            }
        }
        val lastItem = itemsList.last()
        returnList.add(lastItem)
        val item = processWhiteSpaceLogic(lastItem, null, endTime)
        item?.also {
            returnList.add(it)
        }
        return returnList
    }

    private fun getInitialWhiteSpace(
        rowNum: Int,
        itemStartTime: LocalTime,
        startTime: LocalTime
    ): TaskViewItem {
        return TaskViewItem(
            null,
            rowNum,
            EMPTY,
            (itemStartTime.hour - startTime.hour) * 60 + startTime.minute,
            null
        )
    }

    private fun processWhiteSpaceLogic(
        lastItem: TaskViewItem,
        item: TaskViewItem?,
        endTime: LocalTime,
    ): TaskViewItem? {
        var width = 0
        lastItem.operationsType?.endTime?.also { lastEndTime ->
            if (item?.rowNum == lastItem.rowNum) {
                item.operationsType?.also { currentItem ->
                    width = ((currentItem.startTime?.hour
                        ?: 0) - lastEndTime.hour) * 60 +
                            ((currentItem.startTime?.minute ?: 0) - lastEndTime.minute)
                }

            } else {
                width = (endTime.hour + 1 - lastEndTime.hour) * 60 -
                        lastEndTime.minute
            }
        }

        return if (width != 0) {
            TaskViewItem(
                operationsType = null,
                rowNum = lastItem.rowNum,
                viewType = EMPTY,
                width = width
            )
        } else {
            null
        }
    }

    private fun setHeaderRow(
        spanCount: Int,
        startTime: LocalTime
    ): ArrayList<HeaderItem> {
        val headerList = ArrayList<HeaderItem>()
        for (i in 0..spanCount / 60 / 2) {
            val hoursToAdd: Long = (i * 2).toLong()
            val headerTime = startTime.plusHours(hoursToAdd)
            headerList.add(
                HeaderItem(
                    width = HEADER_WIDTH,
                    headerTime = String.format("%02d",headerTime.hour)
                )
            )
        }
        return headerList
    }

    data class ProcessedDataResult(
        val spanCount: Int,
        val employeeList: List<WorkCenterEmployeesType>,
        val operationsList: List<TaskViewItem>,
        val headerList: List<HeaderItem>
    )
}