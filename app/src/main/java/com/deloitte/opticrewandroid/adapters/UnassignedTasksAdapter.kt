package com.deloitte.opticrewandroid.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Worker
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.Utils
import com.deloitte.opticrewandroid.configurations.Constants.DATA_AND_PERSON_ROW_HIGH
import com.deloitte.opticrewandroid.data.AddWorkerItem
import com.deloitte.opticrewandroid.data.TaskViewItem
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType

class UnassignedTasksAdapter(
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var unassignedTaskList: List<OperationsType> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
           return UnassignedTasksViewHolder(
                layoutInflater.inflate(
                    R.layout.unassigned_tasks_popup_row_layout,
                    parent,
                    false
                )
            )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as UnassignedTasksViewHolder).bind(unassignedTaskList[position], context)
    }

    override fun getItemCount(): Int {
        return unassignedTaskList.size
    }

    fun setData(data: List<OperationsType>) {
        unassignedTaskList = data
        notifyDataSetChanged()
    }


    class UnassignedTasksViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val assign: RadioButton? = itemView.findViewById(R.id.add_workers_radio_button)

        fun bind(item: OperationsType, context: Context) {

        }
    }
}