package com.deloitte.opticrewandroid.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.Utils
import com.deloitte.opticrewandroid.configurations.Constants.DATA_AND_PERSON_ROW_HIGH
import com.deloitte.opticrewandroid.configurations.Constants.PERSON_ROW_WIDTH
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType

class EmployeeListAdapter(
    private val onItemClickListener: (employee: WorkCenterEmployeesType) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var employeesList: List<WorkCenterEmployeesType> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return WorkCenterEmployeeViewHolder(
            layoutInflater.inflate(
                R.layout.operations_person_row_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { onItemClickListener.invoke(employeesList.get(position)) }
        (holder as WorkCenterEmployeeViewHolder).bind(employeesList[position], context)
    }

    override fun getItemCount(): Int {
        return employeesList.size
    }

    fun setData(data: List<WorkCenterEmployeesType>) {
        employeesList = data
        notifyDataSetChanged()
    }

    fun getItemAt(pos: Int): WorkCenterEmployeesType {
        return employeesList[pos]
    }

    class WorkCenterEmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val employeeName: TextView? = itemView.findViewById(R.id.employeeName)
        private val avatar: ImageView? = itemView.findViewById(R.id.avatar)
        private val employeeSkills: TextView? = itemView.findViewById(R.id.employeeSkills)
        val viewForeground : ConstraintLayout? = itemView.findViewById(R.id.viewForeground)

        fun bind(item: WorkCenterEmployeesType, context: Context) {
            itemView.layoutParams = LinearLayoutCompat.LayoutParams(
                Utils.pixelsFromDps(PERSON_ROW_WIDTH, context),
                Utils.pixelsFromDps(DATA_AND_PERSON_ROW_HIGH, context)
            )
            // TODO - Change to a real picture when available
            avatar?.setImageResource(R.drawable.ic_android_blue)
            employeeName?.text = item.personName
            employeeSkills?.text = item.qualification
        }
    }
}