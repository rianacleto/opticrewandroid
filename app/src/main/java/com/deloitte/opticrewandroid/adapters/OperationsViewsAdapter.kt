package com.deloitte.opticrewandroid.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.Utils
import com.deloitte.opticrewandroid.configurations.Constants.DATA_AND_PERSON_ROW_HIGH
import com.deloitte.opticrewandroid.configurations.Constants.MIN_TO_DPS_RATIO
import com.deloitte.opticrewandroid.data.TaskViewItem

enum class OperationsViewType(val value: Int) {
    TASK(0), EMPTY(1)
}

class OperationsViewsAdapter(
    private val onItemClickListener: (taskItem: TaskViewItem) -> Unit,
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var operationList: List<TaskViewItem> = emptyList()

    override fun getItemViewType(position: Int): Int {
        return operationList[position].viewType.value
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            OperationsViewType.EMPTY.value -> OperationsViewHolder(
                layoutInflater.inflate(
                    R.layout.operations_empty_row_layout,
                    parent,
                    false
                )
            )
            OperationsViewType.TASK.value -> OperationsViewHolder(
                layoutInflater.inflate(
                    R.layout.operations_task_row_layout,
                    parent,
                    false
                )
            )
            else -> OperationsViewHolder(
                layoutInflater.inflate(
                    R.layout.operations_empty_row_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (operationList[position].viewType == OperationsViewType.TASK) {
            holder.itemView.setOnClickListener {
                onItemClickListener.invoke(
                    operationList.get(
                        position
                    )
                )
            }
        }
        (holder as OperationsViewHolder).bind(operationList[position], context)
    }

    override fun getItemCount(): Int {
        return operationList.size
    }

    fun setData(data: List<TaskViewItem>) {
        operationList = data
        notifyDataSetChanged()
    }

    fun getItemAt(pos: Int): TaskViewItem {
        return operationList[pos]
    }

    class OperationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val taskName: TextView? = itemView.findViewById(R.id.taskName)
        private val employeeName: TextView? = itemView.findViewById(R.id.employeeName)
        private val startEndDate: TextView? = itemView.findViewById(R.id.taskStartAndEnd)


        fun bind(item: TaskViewItem, context: Context) {
            itemView.layoutParams = LinearLayoutCompat.LayoutParams(
                Utils.pixelsFromDps(item.width * MIN_TO_DPS_RATIO, context),
                Utils.pixelsFromDps(DATA_AND_PERSON_ROW_HIGH, context)
            )
            when (item.viewType) {
                OperationsViewType.TASK -> {
                    item.operationsType?.also { operation ->
                        employeeName?.text = operation.personName
                        taskName?.text = operation.operationDescription
                        startEndDate?.text =
                            context.getString(R.string.task_start_and_end_date).format(
                                operation.startTime?.hour,
                                operation.startTime?.minute,
                                operation.endTime?.hour,
                                operation.endTime?.minute
                            )
                    }
                }
            }
        }
    }
}