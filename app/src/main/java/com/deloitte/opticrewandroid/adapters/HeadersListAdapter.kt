package com.deloitte.opticrewandroid.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.Utils
import com.deloitte.opticrewandroid.configurations.Constants.HEADER_AND_EMPTY_HEIGH
import com.deloitte.opticrewandroid.configurations.Constants.MIN_TO_DPS_RATIO
import com.deloitte.opticrewandroid.data.HeaderItem
import com.deloitte.opticrewandroid.usecases.EmployeeToOperationsMergeUseCase
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType

class HeadersListAdapter(
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var headersList: List<HeaderItem> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return WorkCenterEmployeeViewHolder(
            layoutInflater.inflate(
                R.layout.operations_header_row_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as WorkCenterEmployeeViewHolder).bind(headersList[position], context)
    }

    override fun getItemCount(): Int {
        return headersList.size
    }

    fun setData(data: List<HeaderItem>) {
        headersList = data
        notifyDataSetChanged()
    }

    fun getItemAt(pos: Int): HeaderItem {
        return headersList[pos]
    }

    class WorkCenterEmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // header
        private val hour: TextView? = itemView.findViewById(R.id.hour)

        fun bind(item: HeaderItem, context: Context) {
            itemView.layoutParams = LinearLayoutCompat.LayoutParams(
                Utils.pixelsFromDps(item.width * MIN_TO_DPS_RATIO, context),
                LinearLayoutCompat.LayoutParams.MATCH_PARENT
            )
            hour?.text = item.headerTime
        }
    }
}
