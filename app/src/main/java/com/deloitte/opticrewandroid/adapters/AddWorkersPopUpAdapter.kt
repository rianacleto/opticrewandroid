package com.deloitte.opticrewandroid.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Worker
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.Utils
import com.deloitte.opticrewandroid.configurations.Constants.DATA_AND_PERSON_ROW_HIGH
import com.deloitte.opticrewandroid.data.AddWorkerItem
import com.deloitte.opticrewandroid.data.TaskViewItem

enum class AddWorkersPopUpViewType(val value: Int) {
    TITLE(0), WORKER(1)
}

class AddWorkersPopUpAdapter(
    private val context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var workerList: List<AddWorkerItem> = emptyList()

    override fun getItemViewType(position: Int): Int {
        return workerList[position].viewType.value
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            AddWorkersPopUpViewType.TITLE.value -> AddWorkerViewHolder(
                layoutInflater.inflate(
                    R.layout.add_workers_popup_title_row_layout,
                    parent,
                    false
                )
            )
            AddWorkersPopUpViewType.WORKER.value -> AddWorkerViewHolder(
                layoutInflater.inflate(
                    R.layout.add_workers_popup_worker_row_layout,
                    parent,
                    false
                )
            )
            else -> AddWorkerViewHolder(
                layoutInflater.inflate(
                    R.layout.add_workers_popup_title_row_layout,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AddWorkerViewHolder).bind(workerList[position], context)
    }

    override fun getItemCount(): Int {
        return workerList.size
    }

    fun setData(data: List<AddWorkerItem>) {
        workerList = data
        notifyDataSetChanged()
    }


    class AddWorkerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val radioButton: RadioButton? = itemView.findViewById(R.id.add_workers_radio_button)
        private val workerName: TextView? = itemView.findViewById(R.id.add_workers_worker_name)
        private val workerSkill: TextView? = itemView.findViewById(R.id.add_workers_worker_skill)
        private val workerStatus: TextView? = itemView.findViewById(R.id.add_workers_status)
        private val title: TextView? = itemView.findViewById(R.id.add_workers_title)

        fun bind(item: AddWorkerItem, context: Context) {
            when (item.viewType) {
                AddWorkersPopUpViewType.WORKER -> {
                    itemView.setOnClickListener {
                        item.isChecked = item.isChecked
                        radioButton?.also {
                            it.isChecked = !it.isChecked
                        }

                    }
                    workerName?.setText(item.workerName)
                    workerSkill?.setText(item.skill)
                    workerStatus?.also {
                        when (item.status) {
                            WorkerStatus.BUSY -> {
                                it.setText(R.string.busy)
                                it.setTextColor(ContextCompat.getColor(context,R.color.add_worker_red))
                            }
                            WorkerStatus.TENTATIVE -> {
                                it.setText(R.string.tentative)
                                it.setTextColor(ContextCompat.getColor(context,R.color.add_worker_orange))
                            }
                            WorkerStatus.AVAILABLE -> {
                                it.setText(R.string.available)
                                it.setTextColor(ContextCompat.getColor(context,R.color.add_worker_green))
                            }
                        }
                    }
                }
                AddWorkersPopUpViewType.TITLE -> title?.setText(item.title)
            }
        }
    }

    enum class WorkerStatus { BUSY, TENTATIVE, AVAILABLE }
}