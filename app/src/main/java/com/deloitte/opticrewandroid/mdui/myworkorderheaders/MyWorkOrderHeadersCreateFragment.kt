package com.deloitte.opticrewandroid.mdui.myworkorderheaders

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.databinding.FragmentMyworkorderheadersCreateBinding
import com.deloitte.opticrewandroid.mdui.BundleKeys
import com.deloitte.opticrewandroid.mdui.InterfacedFragment
import com.deloitte.opticrewandroid.mdui.UIConstants
import com.deloitte.opticrewandroid.repository.OperationResult
import com.deloitte.opticrewandroid.viewmodel.myworkorderheaderstype.MyWorkOrderHeadersTypeViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyWorkOrderHeadersType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntityTypes
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets
import com.sap.cloud.mobile.fiori.formcell.SimplePropertyFormCell
import com.sap.cloud.mobile.fiori.`object`.ObjectHeader
import com.sap.cloud.mobile.odata.Property
import org.slf4j.LoggerFactory

/**
 * A fragment that is used for both update and create for users to enter values for the properties. When used for
 * update, an instance of the entity is required. In the case of create, a new instance of the entity with defaults will
 * be created. The default values may not be acceptable for the OData service.
 * This fragment is either contained in a [MyWorkOrderHeadersListActivity] in two-pane mode (on tablets) or a
 * [MyWorkOrderHeadersDetailActivity] on handsets.
 *
 * Arguments: Operation: [OP_CREATE | OP_UPDATE]
 *            MyWorkOrderHeadersType if Operation is update
 */
class MyWorkOrderHeadersCreateFragment : InterfacedFragment<MyWorkOrderHeadersType>() {

    /** MyWorkOrderHeadersType object and it's copy: the modifications are done on the copied object. */
    private lateinit var myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType
    private lateinit var myWorkOrderHeadersTypeEntityCopy: MyWorkOrderHeadersType

    /** DataBinding generated class */
    private lateinit var binding: FragmentMyworkorderheadersCreateBinding

    /** Indicate what operation to be performed */
    private lateinit var operation: String

    /** myWorkOrderHeadersTypeEntity ViewModel */
    private lateinit var viewModel: MyWorkOrderHeadersTypeViewModel

    /** The update menu item */
    private lateinit var updateMenuItem: MenuItem

    private val isMyWorkOrderHeadersTypeValid: Boolean
        get() {
            var isValid = true
            view?.findViewById<LinearLayout>(R.id.create_update_myworkorderheaderstype)?.let { linearLayout ->
                for (i in 0 until linearLayout.childCount) {
                    val simplePropertyFormCell = linearLayout.getChildAt(i) as SimplePropertyFormCell
                    val propertyName = simplePropertyFormCell.tag as String
                    val property = EntityTypes.myWorkOrderHeadersType.getProperty(propertyName)
                    val value = simplePropertyFormCell.value.toString()
                    if (!isValidProperty(property, value)) {
                        simplePropertyFormCell.setTag(R.id.TAG_HAS_MANDATORY_ERROR, true)
                        val errorMessage = resources.getString(R.string.mandatory_warning)
                        simplePropertyFormCell.isErrorEnabled = true
                        simplePropertyFormCell.error = errorMessage
                        isValid = false
                    } else {
                        if (simplePropertyFormCell.isErrorEnabled) {
                            val hasMandatoryError = simplePropertyFormCell.getTag(R.id.TAG_HAS_MANDATORY_ERROR) as Boolean
                            if (!hasMandatoryError) {
                                isValid = false
                            } else {
                                simplePropertyFormCell.isErrorEnabled = false
                            }
                        }
                        simplePropertyFormCell.setTag(R.id.TAG_HAS_MANDATORY_ERROR, false)
                    }
                }
            }
            return isValid
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        menu = R.menu.itemlist_edit_options
        setHasOptionsMenu(true)

        arguments?.let {
            (it.getString(BundleKeys.OPERATION))?.let { operationType ->
                operation = operationType
                activityTitle = when (operationType) {
                    UIConstants.OP_CREATE -> resources.getString(R.string.title_create_fragment, EntityTypes.myWorkOrderHeadersType.localName)
                    else -> resources.getString(R.string.title_update_fragment) + " " + EntityTypes.myWorkOrderHeadersType.localName

                }
            }
        }

        activity?.let {
            (it as MyWorkOrderHeadersActivity).isNavigationDisabled = true
            viewModel = ViewModelProvider(it).get(MyWorkOrderHeadersTypeViewModel::class.java)
            viewModel.createResult.observe(this, Observer { result -> onComplete(result!!) })
            viewModel.updateResult.observe(this, Observer { result -> onComplete(result!!) })

            if (operation == UIConstants.OP_CREATE) {
                myWorkOrderHeadersTypeEntity = createMyWorkOrderHeadersType()
            } else {
                myWorkOrderHeadersTypeEntity = viewModel.selectedEntity.value!!
            }

            val workingCopy = savedInstanceState?.getParcelable<MyWorkOrderHeadersType>(KEY_WORKING_COPY)
            if (workingCopy == null) {
                myWorkOrderHeadersTypeEntityCopy = myWorkOrderHeadersTypeEntity.copy()
                myWorkOrderHeadersTypeEntityCopy.setEntityTag(myWorkOrderHeadersTypeEntity.getEntityTag())
                myWorkOrderHeadersTypeEntityCopy.setOldEntity(myWorkOrderHeadersTypeEntity)
                myWorkOrderHeadersTypeEntityCopy.editLink = myWorkOrderHeadersTypeEntity.editLink
            } else {
                myWorkOrderHeadersTypeEntityCopy = workingCopy
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        currentActivity.findViewById<ObjectHeader>(R.id.objectHeader)?.let {
            it.visibility = View.GONE
        }
        val rootView = setupDataBinding(inflater, container)
        return rootView
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_item -> {
                updateMenuItem = item
                enableUpdateMenuItem(false)
                onSaveItem()
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if(secondaryToolbar != null) secondaryToolbar!!.setTitle(activityTitle) else activity?.setTitle(activityTitle)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(KEY_WORKING_COPY, myWorkOrderHeadersTypeEntityCopy)
        super.onSaveInstanceState(outState)
    }

    /** Enables the update menu item based on [enable] */
    private fun enableUpdateMenuItem(enable : Boolean = true) {
        updateMenuItem.also {
            it.isEnabled = enable
            it.icon.alpha = if(enable) 255 else 130
        }
    }

    /** Saves the entity */
    private fun onSaveItem(): Boolean {
        if (!isMyWorkOrderHeadersTypeValid) {
            return false
        }
        (currentActivity as MyWorkOrderHeadersActivity).isNavigationDisabled = false
        progressBar?.visibility = View.VISIBLE
        when (operation) {
            UIConstants.OP_CREATE -> {
                viewModel.create(myWorkOrderHeadersTypeEntityCopy)
            }
            UIConstants.OP_UPDATE -> viewModel.update(myWorkOrderHeadersTypeEntityCopy)
        }
        return true
    }

    /**
     * Create a new MyWorkOrderHeadersType instance and initialize properties to its default values
     * Nullable property will remain null
     * @return new MyWorkOrderHeadersType instance
     */
    private fun createMyWorkOrderHeadersType(): MyWorkOrderHeadersType {
        val entity = MyWorkOrderHeadersType(true)
        return entity
    }

    /** Callback function to complete processing when updateResult or createResult events fired */
    private fun onComplete(result: OperationResult<MyWorkOrderHeadersType>) {
        progressBar?.visibility = View.INVISIBLE
        enableUpdateMenuItem(true)
        if (result.error != null) {
            (currentActivity as MyWorkOrderHeadersActivity).isNavigationDisabled = true
            handleError(result)
        } else {
            if (operation == UIConstants.OP_UPDATE && !currentActivity.resources.getBoolean(R.bool.two_pane)) {
                viewModel.selectedEntity.value = myWorkOrderHeadersTypeEntityCopy
            }
            (currentActivity as MyWorkOrderHeadersActivity).onBackPressed()
        }
    }

    /** Simple validation: checks the presence of mandatory fields. */
    private fun isValidProperty(property: Property, value: String): Boolean {
        return !(!property.isNullable && value.isEmpty())
    }

    /**
     * Set up data binding for this view
     *
     * @param [inflater] layout inflater from onCreateView
     * @param [container] view group from onCreateView
     *
     * @return rootView from generated data binding code
     */
    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentMyworkorderheadersCreateBinding.inflate(inflater, container, false)
        binding.setMyWorkOrderHeadersType(myWorkOrderHeadersTypeEntityCopy)
        return binding.root
    }

    /**
     * Notify user of error encountered while execution the operation
     *
     * @param [result] operation result with error
     */
    private fun handleError(result: OperationResult<MyWorkOrderHeadersType>) {
        val errorMessage = when (result.operation) {
            OperationResult.Operation.UPDATE -> getString(R.string.update_failed_detail)
            OperationResult.Operation.CREATE -> getString(R.string.create_failed_detail)
            else -> throw AssertionError()
        }
        showError(errorMessage)
    }


    companion object {
        private val KEY_WORKING_COPY = "WORKING_COPY"
        private val LOGGER = LoggerFactory.getLogger(MyWorkOrderHeadersActivity::class.java)
    }
}
