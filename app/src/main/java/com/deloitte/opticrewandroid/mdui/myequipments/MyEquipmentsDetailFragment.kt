package com.deloitte.opticrewandroid.mdui.myequipments

import androidx.lifecycle.Observer
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import com.deloitte.opticrewandroid.service.SAPServiceManager
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.databinding.FragmentMyequipmentsDetailBinding
import com.deloitte.opticrewandroid.mdui.EntityKeyUtil
import com.deloitte.opticrewandroid.mdui.InterfacedFragment
import com.deloitte.opticrewandroid.mdui.UIConstants
import com.deloitte.opticrewandroid.repository.OperationResult
import com.deloitte.opticrewandroid.viewmodel.myequipmentstype.MyEquipmentsTypeViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets;
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyEquipmentsType
import com.sap.cloud.mobile.fiori.indicator.FioriProgressBar
import com.sap.cloud.mobile.fiori.`object`.ObjectHeader
import kotlinx.android.synthetic.main.activity_entityitem.view.*

import com.deloitte.opticrewandroid.mdui.myequipments.MyEquipmentsActivity
import com.deloitte.opticrewandroid.mdui.myworkorderheaders.MyWorkOrderHeadersActivity
import com.deloitte.opticrewandroid.mdui.myworkorderoperations.MyWorkOrderOperationsActivity

/**
 * A fragment representing a single MyEquipmentsType detail screen.
 * This fragment is contained in an MyEquipmentsActivity.
 */
class MyEquipmentsDetailFragment : InterfacedFragment<MyEquipmentsType>() {

    /** Generated data binding class based on layout file */
    private lateinit var binding: FragmentMyequipmentsDetailBinding

    /** MyEquipmentsType entity to be displayed */
    private lateinit var myEquipmentsTypeEntity: MyEquipmentsType

    /** Fiori ObjectHeader component used when entity is to be displayed on phone */
    private var objectHeader: ObjectHeader? = null

    /** View model of the entity type that the displayed entity belongs to */
    private lateinit var viewModel: MyEquipmentsTypeViewModel
    /**
     * Service manager to provide root URL of OData Service for Glide to load images if there are media resources
     * associated with the entity type
     */
    private var sapServiceManager: SAPServiceManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        menu = R.menu.itemlist_view_options
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return setupDataBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            currentActivity = it
            viewModel = ViewModelProvider(it).get(MyEquipmentsTypeViewModel::class.java)
            viewModel.deleteResult.observe(viewLifecycleOwner, Observer { result ->
                onDeleteComplete(result!!)
            })

            viewModel.selectedEntity.observe(viewLifecycleOwner, Observer { entity ->
                myEquipmentsTypeEntity = entity
                binding.setMyEquipmentsType(entity)
                setupObjectHeader()
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.update_item -> {
                listener?.onFragmentStateChange(UIConstants.EVENT_EDIT_ITEM, myEquipmentsTypeEntity)
                true
            }
            R.id.delete_item -> {
                listener?.onFragmentStateChange(UIConstants.EVENT_ASK_DELETE_CONFIRMATION,null)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Completion callback for delete operation
     *
     * @param [result] of the operation
     */
    private fun onDeleteComplete(result: OperationResult<MyEquipmentsType>) {
        progressBar?.let {
            it.visibility = View.INVISIBLE
        }
        viewModel.removeAllSelected()
        result.error?.let {
            showError(getString(R.string.delete_failed_detail))
            return
        }
        listener?.onFragmentStateChange(UIConstants.EVENT_DELETION_COMPLETED, myEquipmentsTypeEntity)
    }


    @Suppress("UNUSED", "UNUSED_PARAMETER") // parameter is needed because of the xml binding
    fun onNavigationClickedToMyEquipments_to_FunctionalLocations(view: View) {
        val intent = Intent(currentActivity, MyEquipmentsActivity::class.java)
        intent.putExtra("parent", myEquipmentsTypeEntity)
        intent.putExtra("navigation", "to_FunctionalLocations")
        startActivity(intent)
    }

    @Suppress("UNUSED", "UNUSED_PARAMETER") // parameter is needed because of the xml binding
    fun onNavigationClickedToMyWorkOrderHeaders_to_WOHeader(view: View) {
        val intent = Intent(currentActivity, MyWorkOrderHeadersActivity::class.java)
        intent.putExtra("parent", myEquipmentsTypeEntity)
        intent.putExtra("navigation", "to_WOHeader")
        startActivity(intent)
    }

    @Suppress("UNUSED", "UNUSED_PARAMETER") // parameter is needed because of the xml binding
    fun onNavigationClickedToMyWorkOrderOperations_to_WorkOrderOperation(view: View) {
        val intent = Intent(currentActivity, MyWorkOrderOperationsActivity::class.java)
        intent.putExtra("parent", myEquipmentsTypeEntity)
        intent.putExtra("navigation", "to_WorkOrderOperation")
        startActivity(intent)
    }

    /**
     * Set up databinding for this view
     *
     * @param [inflater] layout inflater from onCreateView
     * @param [container] view group from onCreateView
     *
     * @return [View] rootView from generated databinding code
     */
    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentMyequipmentsDetailBinding.inflate(inflater, container, false)
        binding.handler = this
        return binding.root
    }

    /**
     * Set detail image of ObjectHeader.
     * When the entity does not provides picture, set the first character of the masterProperty.
     */
    private fun setDetailImage(objectHeader: ObjectHeader, myEquipmentsTypeEntity: MyEquipmentsType) {
        if (myEquipmentsTypeEntity.getDataValue(MyEquipmentsType.equipDesc) != null && !myEquipmentsTypeEntity.getDataValue(MyEquipmentsType.equipDesc).toString().isEmpty()) {
            objectHeader.detailImageCharacter = myEquipmentsTypeEntity.getDataValue(MyEquipmentsType.equipDesc).toString().substring(0, 1)
        } else {
            objectHeader.detailImageCharacter = "?"
        }
    }

    /**
     * Setup ObjectHeader with an instance of myEquipmentsTypeEntity
     */
    private fun setupObjectHeader() {
        val secondToolbar = currentActivity.findViewById<Toolbar>(R.id.secondaryToolbar)
        if (secondToolbar != null) {
            secondToolbar.setTitle(myEquipmentsTypeEntity.entityType.localName)
        } else {
            currentActivity.setTitle(myEquipmentsTypeEntity.entityType.localName)
        }

        // Object Header is not available in tablet mode
        objectHeader = currentActivity.findViewById(R.id.objectHeader)
        val dataValue = myEquipmentsTypeEntity.getDataValue(MyEquipmentsType.equipDesc)

        objectHeader?.let {
            it.apply {
                headline = dataValue?.toString()
                subheadline = EntityKeyUtil.getOptionalEntityKey(myEquipmentsTypeEntity)
                body = "You can set the header body text here."
                footnote = "You can set the header footnote here."
                description = "You can add a detailed item description here."
            }
            it.setTag("#tag1", 0)
            it.setTag("#tag3", 2)
            it.setTag("#tag2", 1)

            setDetailImage(it, myEquipmentsTypeEntity)
            it.visibility = View.VISIBLE
        }
    }
}
