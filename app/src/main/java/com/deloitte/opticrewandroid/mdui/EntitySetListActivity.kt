package com.deloitte.opticrewandroid.mdui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.*
import android.widget.ArrayAdapter
import android.content.Context
import android.content.Intent

import java.util.ArrayList
import java.util.HashMap
import com.deloitte.opticrewandroid.mdui.employees.EmployeesActivity
import com.deloitte.opticrewandroid.mdui.materials.MaterialsActivity
import com.deloitte.opticrewandroid.mdui.myequipments.MyEquipmentsActivity
import com.deloitte.opticrewandroid.mdui.myfunctionallocations.MyFunctionalLocationsActivity
import com.deloitte.opticrewandroid.mdui.myworkcenters.MyWorkCentersActivity
import com.deloitte.opticrewandroid.mdui.myworkorderheaders.MyWorkOrderHeadersActivity
import com.deloitte.opticrewandroid.mdui.myworkorderoperations.MyWorkOrderOperationsActivity
import com.deloitte.opticrewandroid.mdui.operationcomponents.OperationComponentsActivity
import com.deloitte.opticrewandroid.mdui.operations.OperationsActivity
import com.deloitte.opticrewandroid.mdui.workcenteremployees.WorkCenterEmployeesActivity
import com.deloitte.opticrewandroid.mdui.workcenters.WorkCentersActivity
import org.slf4j.LoggerFactory
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.app.TasksViewActivity
import com.sap.cloud.mobile.foundation.common.ClientProvider
import com.sap.cloud.mobile.odata.OnlineODataProvider
import com.sap.cloud.mobile.odata.http.OKHttpHandler

import kotlinx.android.synthetic.main.activity_entity_set_list.*
import kotlinx.android.synthetic.main.element_entity_set_list.view.*

/*
 * An activity to display the list of all entity types from the OData service
 */
class EntitySetListActivity : AppCompatActivity() {
    private val entitySetNames = ArrayList<String>()
    private val entitySetNameMap = HashMap<String, EntitySetName>()


    enum class EntitySetName constructor(
        val entitySetName: String,
        val titleId: Int,
        val iconId: Int
    ) {
        Employees(
            "Employees", R.string.eset_employees,
            BLUE_ANDROID_ICON
        ),
        Materials(
            "Materials", R.string.eset_materials,
            WHITE_ANDROID_ICON
        ),
        MyEquipments(
            "MyEquipments", R.string.eset_myequipments,
            BLUE_ANDROID_ICON
        ),
        MyFunctionalLocations(
            "MyFunctionalLocations", R.string.eset_myfunctionallocations,
            WHITE_ANDROID_ICON
        ),
        MyWorkCenters(
            "MyWorkCenters", R.string.eset_myworkcenters,
            BLUE_ANDROID_ICON
        ),
        MyWorkOrderHeaders(
            "MyWorkOrderHeaders", R.string.eset_myworkorderheaders,
            WHITE_ANDROID_ICON
        ),
        MyWorkOrderOperations(
            "MyWorkOrderOperations", R.string.eset_myworkorderoperations,
            BLUE_ANDROID_ICON
        ),
        OperationComponents(
            "OperationComponents", R.string.eset_operationcomponents,
            WHITE_ANDROID_ICON
        ),
        Operations(
            "Operations", R.string.eset_operations,
            BLUE_ANDROID_ICON
        ),
        WorkCenterEmployees(
            "WorkCenterEmployees", R.string.eset_workcenteremployees,
            WHITE_ANDROID_ICON
        ),
        WorkCenters(
            "WorkCenters", R.string.eset_workcenters,
            BLUE_ANDROID_ICON
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entity_set_list)
        val toolbar = findViewById<Toolbar>(R.id.toolbar) // to avoid ambiguity
        setSupportActionBar(toolbar)

        entitySetNames.clear()
        entitySetNameMap.clear()
        for (entitySet in EntitySetName.values()) {
            val entitySetTitle = resources.getString(entitySet.titleId)
            entitySetNames.add(entitySetTitle)
            entitySetNameMap[entitySetTitle] = entitySet
        }

        val listView = entity_list
        val adapter = EntitySetListAdapter(this, R.layout.element_entity_set_list, entitySetNames)

        testBtn.setOnClickListener { startActivity(Intent(this, TasksViewActivity::class.java)) }

        listView.adapter = adapter

        listView.setOnItemClickListener listView@{ _, _, position, _ ->
            val entitySetName = entitySetNameMap[adapter.getItem(position)!!]
            val context = this@EntitySetListActivity
            val intent: Intent = when (entitySetName) {
                EntitySetName.Employees -> Intent(context, EmployeesActivity::class.java)
                EntitySetName.Materials -> Intent(context, MaterialsActivity::class.java)
                EntitySetName.MyEquipments -> Intent(context, MyEquipmentsActivity::class.java)
                EntitySetName.MyFunctionalLocations -> Intent(
                    context,
                    MyFunctionalLocationsActivity::class.java
                )
                EntitySetName.MyWorkCenters -> Intent(context, MyWorkCentersActivity::class.java)
                EntitySetName.MyWorkOrderHeaders -> Intent(
                    context,
                    MyWorkOrderHeadersActivity::class.java
                )
                EntitySetName.MyWorkOrderOperations -> Intent(
                    context,
                    MyWorkOrderOperationsActivity::class.java
                )
                EntitySetName.OperationComponents -> Intent(
                    context,
                    OperationComponentsActivity::class.java
                )
                EntitySetName.Operations -> Intent(context, OperationsActivity::class.java)
                EntitySetName.WorkCenterEmployees -> Intent(
                    context,
                    WorkCenterEmployeesActivity::class.java
                )
                EntitySetName.WorkCenters -> Intent(context, WorkCentersActivity::class.java)
                else -> return@listView
            }
            context.startActivity(intent)
        }
    }

    inner class EntitySetListAdapter internal constructor(
        context: Context,
        resource: Int,
        entitySetNames: List<String>
    ) : ArrayAdapter<String>(context, resource, entitySetNames) {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view = convertView
            val entitySetName = entitySetNameMap[getItem(position)!!]
            if (view == null) {
                view = LayoutInflater.from(context)
                    .inflate(R.layout.element_entity_set_list, parent, false)
            }
            val entitySetCell = view!!.entity_set_name
            entitySetCell.headline = entitySetName?.titleId?.let {
                context.resources.getString(it)
            }
            entitySetName?.iconId?.let { entitySetCell.setDetailImage(it) }
            return view
        }
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.entity_set_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        LOGGER.debug("onOptionsItemSelected: " + item.title)
        return when (item.itemId) {
            R.id.menu_settings -> {
                LOGGER.debug("settings screen menu item selected.")
                Intent(this, SettingsActivity::class.java).also {
                    this.startActivity(it)
                }
                true
            }
            else -> false
        }
    }


    companion object {
        private val LOGGER = LoggerFactory.getLogger(EntitySetListActivity::class.java)
        private const val BLUE_ANDROID_ICON = R.drawable.ic_android_blue
        private const val WHITE_ANDROID_ICON = R.drawable.ic_android_white
    }
}
