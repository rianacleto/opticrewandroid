package com.deloitte.opticrewandroid.mdui.workcenters

import androidx.lifecycle.Observer
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import com.deloitte.opticrewandroid.service.SAPServiceManager
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.databinding.FragmentWorkcentersDetailBinding
import com.deloitte.opticrewandroid.mdui.EntityKeyUtil
import com.deloitte.opticrewandroid.mdui.InterfacedFragment
import com.deloitte.opticrewandroid.mdui.UIConstants
import com.deloitte.opticrewandroid.repository.OperationResult
import com.deloitte.opticrewandroid.viewmodel.workcenterstype.WorkCentersTypeViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets;
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCentersType
import com.sap.cloud.mobile.fiori.indicator.FioriProgressBar
import com.sap.cloud.mobile.fiori.`object`.ObjectHeader
import kotlinx.android.synthetic.main.activity_entityitem.view.*


/**
 * A fragment representing a single WorkCentersType detail screen.
 * This fragment is contained in an WorkCentersActivity.
 */
class WorkCentersDetailFragment : InterfacedFragment<WorkCentersType>() {

    /** Generated data binding class based on layout file */
    private lateinit var binding: FragmentWorkcentersDetailBinding

    /** WorkCentersType entity to be displayed */
    private lateinit var workCentersTypeEntity: WorkCentersType

    /** Fiori ObjectHeader component used when entity is to be displayed on phone */
    private var objectHeader: ObjectHeader? = null

    /** View model of the entity type that the displayed entity belongs to */
    private lateinit var viewModel: WorkCentersTypeViewModel
    /**
     * Service manager to provide root URL of OData Service for Glide to load images if there are media resources
     * associated with the entity type
     */
    private var sapServiceManager: SAPServiceManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        menu = R.menu.itemlist_view_options
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return setupDataBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            currentActivity = it
            viewModel = ViewModelProvider(it).get(WorkCentersTypeViewModel::class.java)
            viewModel.deleteResult.observe(viewLifecycleOwner, Observer { result ->
                onDeleteComplete(result!!)
            })

            viewModel.selectedEntity.observe(viewLifecycleOwner, Observer { entity ->
                workCentersTypeEntity = entity
                binding.setWorkCentersType(entity)
                setupObjectHeader()
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.update_item -> {
                listener?.onFragmentStateChange(UIConstants.EVENT_EDIT_ITEM, workCentersTypeEntity)
                true
            }
            R.id.delete_item -> {
                listener?.onFragmentStateChange(UIConstants.EVENT_ASK_DELETE_CONFIRMATION,null)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Completion callback for delete operation
     *
     * @param [result] of the operation
     */
    private fun onDeleteComplete(result: OperationResult<WorkCentersType>) {
        progressBar?.let {
            it.visibility = View.INVISIBLE
        }
        viewModel.removeAllSelected()
        result.error?.let {
            showError(getString(R.string.delete_failed_detail))
            return
        }
        listener?.onFragmentStateChange(UIConstants.EVENT_DELETION_COMPLETED, workCentersTypeEntity)
    }


    /**
     * Set up databinding for this view
     *
     * @param [inflater] layout inflater from onCreateView
     * @param [container] view group from onCreateView
     *
     * @return [View] rootView from generated databinding code
     */
    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentWorkcentersDetailBinding.inflate(inflater, container, false)
        binding.handler = this
        return binding.root
    }

    /**
     * Set detail image of ObjectHeader.
     * When the entity does not provides picture, set the first character of the masterProperty.
     */
    private fun setDetailImage(objectHeader: ObjectHeader, workCentersTypeEntity: WorkCentersType) {
        if (workCentersTypeEntity.getDataValue(WorkCentersType.workCentreName) != null && !workCentersTypeEntity.getDataValue(WorkCentersType.workCentreName).toString().isEmpty()) {
            objectHeader.detailImageCharacter = workCentersTypeEntity.getDataValue(WorkCentersType.workCentreName).toString().substring(0, 1)
        } else {
            objectHeader.detailImageCharacter = "?"
        }
    }

    /**
     * Setup ObjectHeader with an instance of workCentersTypeEntity
     */
    private fun setupObjectHeader() {
        val secondToolbar = currentActivity.findViewById<Toolbar>(R.id.secondaryToolbar)
        if (secondToolbar != null) {
            secondToolbar.setTitle(workCentersTypeEntity.entityType.localName)
        } else {
            currentActivity.setTitle(workCentersTypeEntity.entityType.localName)
        }

        // Object Header is not available in tablet mode
        objectHeader = currentActivity.findViewById(R.id.objectHeader)
        val dataValue = workCentersTypeEntity.getDataValue(WorkCentersType.workCentreName)

        objectHeader?.let {
            it.apply {
                headline = dataValue?.toString()
                subheadline = EntityKeyUtil.getOptionalEntityKey(workCentersTypeEntity)
                body = "You can set the header body text here."
                footnote = "You can set the header footnote here."
                description = "You can add a detailed item description here."
            }
            it.setTag("#tag1", 0)
            it.setTag("#tag3", 2)
            it.setTag("#tag2", 1)

            setDetailImage(it, workCentersTypeEntity)
            it.visibility = View.VISIBLE
        }
    }
}
