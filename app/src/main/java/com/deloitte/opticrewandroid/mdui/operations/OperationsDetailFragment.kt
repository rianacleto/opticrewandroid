package com.deloitte.opticrewandroid.mdui.operations

import androidx.lifecycle.Observer
import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import com.deloitte.opticrewandroid.service.SAPServiceManager
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.databinding.FragmentOperationsDetailBinding
import com.deloitte.opticrewandroid.mdui.EntityKeyUtil
import com.deloitte.opticrewandroid.mdui.InterfacedFragment
import com.deloitte.opticrewandroid.mdui.UIConstants
import com.deloitte.opticrewandroid.repository.OperationResult
import com.deloitte.opticrewandroid.viewmodel.operationstype.OperationsTypeViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets;
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import com.sap.cloud.mobile.fiori.indicator.FioriProgressBar
import com.sap.cloud.mobile.fiori.`object`.ObjectHeader
import kotlinx.android.synthetic.main.activity_entityitem.view.*

import com.deloitte.opticrewandroid.mdui.operationcomponents.OperationComponentsActivity

/**
 * A fragment representing a single OperationsType detail screen.
 * This fragment is contained in an OperationsActivity.
 */
class OperationsDetailFragment : InterfacedFragment<OperationsType>() {

    /** Generated data binding class based on layout file */
    private lateinit var binding: FragmentOperationsDetailBinding

    /** OperationsType entity to be displayed */
    private lateinit var operationsTypeEntity: OperationsType

    /** Fiori ObjectHeader component used when entity is to be displayed on phone */
    private var objectHeader: ObjectHeader? = null

    /** View model of the entity type that the displayed entity belongs to */
    private lateinit var viewModel: OperationsTypeViewModel
    /**
     * Service manager to provide root URL of OData Service for Glide to load images if there are media resources
     * associated with the entity type
     */
    private var sapServiceManager: SAPServiceManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        menu = R.menu.itemlist_view_options
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return setupDataBinding(inflater, container)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity?.let {
            currentActivity = it
            viewModel = ViewModelProvider(it).get(OperationsTypeViewModel::class.java)
            viewModel.deleteResult.observe(viewLifecycleOwner, Observer { result ->
                onDeleteComplete(result!!)
            })

            viewModel.selectedEntity.observe(viewLifecycleOwner, Observer { entity ->
                operationsTypeEntity = entity
                binding.setOperationsType(entity)
                setupObjectHeader()
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.update_item -> {
                listener?.onFragmentStateChange(UIConstants.EVENT_EDIT_ITEM, operationsTypeEntity)
                true
            }
            R.id.delete_item -> {
                listener?.onFragmentStateChange(UIConstants.EVENT_ASK_DELETE_CONFIRMATION,null)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Completion callback for delete operation
     *
     * @param [result] of the operation
     */
    private fun onDeleteComplete(result: OperationResult<OperationsType>) {
        progressBar?.let {
            it.visibility = View.INVISIBLE
        }
        viewModel.removeAllSelected()
        result.error?.let {
            showError(getString(R.string.delete_failed_detail))
            return
        }
        listener?.onFragmentStateChange(UIConstants.EVENT_DELETION_COMPLETED, operationsTypeEntity)
    }


    @Suppress("UNUSED", "UNUSED_PARAMETER") // parameter is needed because of the xml binding
    fun onNavigationClickedToOperationComponents_to_OperationComponents(view: View) {
        val intent = Intent(currentActivity, OperationComponentsActivity::class.java)
        intent.putExtra("parent", operationsTypeEntity)
        intent.putExtra("navigation", "to_OperationComponents")
        startActivity(intent)
    }

    /**
     * Set up databinding for this view
     *
     * @param [inflater] layout inflater from onCreateView
     * @param [container] view group from onCreateView
     *
     * @return [View] rootView from generated databinding code
     */
    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentOperationsDetailBinding.inflate(inflater, container, false)
        binding.handler = this
        return binding.root
    }

    /**
     * Set detail image of ObjectHeader.
     * When the entity does not provides picture, set the first character of the masterProperty.
     */
    private fun setDetailImage(objectHeader: ObjectHeader, operationsTypeEntity: OperationsType) {
        if (operationsTypeEntity.getDataValue(OperationsType.workCenterPlant) != null && !operationsTypeEntity.getDataValue(OperationsType.workCenterPlant).toString().isEmpty()) {
            objectHeader.detailImageCharacter = operationsTypeEntity.getDataValue(OperationsType.workCenterPlant).toString().substring(0, 1)
        } else {
            objectHeader.detailImageCharacter = "?"
        }
    }

    /**
     * Setup ObjectHeader with an instance of operationsTypeEntity
     */
    private fun setupObjectHeader() {
        val secondToolbar = currentActivity.findViewById<Toolbar>(R.id.secondaryToolbar)
        if (secondToolbar != null) {
            secondToolbar.setTitle(operationsTypeEntity.entityType.localName)
        } else {
            currentActivity.setTitle(operationsTypeEntity.entityType.localName)
        }

        // Object Header is not available in tablet mode
        objectHeader = currentActivity.findViewById(R.id.objectHeader)
        val dataValue = operationsTypeEntity.getDataValue(OperationsType.workCenterPlant)

        objectHeader?.let {
            it.apply {
                headline = dataValue?.toString()
                subheadline = EntityKeyUtil.getOptionalEntityKey(operationsTypeEntity)
                body = "You can set the header body text here."
                footnote = "You can set the header footnote here."
                description = "You can add a detailed item description here."
            }
            it.setTag("#tag1", 0)
            it.setTag("#tag3", 2)
            it.setTag("#tag2", 1)

            setDetailImage(it, operationsTypeEntity)
            it.visibility = View.VISIBLE
        }
    }
}
