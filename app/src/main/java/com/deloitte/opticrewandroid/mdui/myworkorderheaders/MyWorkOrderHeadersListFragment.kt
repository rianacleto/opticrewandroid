package com.deloitte.opticrewandroid.mdui.myworkorderheaders

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.WorkerThread
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.appcompat.view.ActionMode
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.CheckBox
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.deloitte.opticrewandroid.service.SAPServiceManager
import com.deloitte.opticrewandroid.app.SAPWizardApplication
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.viewmodel.EntityViewModelFactory
import com.deloitte.opticrewandroid.viewmodel.myworkorderheaderstype.MyWorkOrderHeadersTypeViewModel
import com.deloitte.opticrewandroid.repository.OperationResult
import com.deloitte.opticrewandroid.mdui.UIConstants
import com.deloitte.opticrewandroid.mdui.EntityKeyUtil
import com.deloitte.opticrewandroid.mdui.EntitySetListActivity.EntitySetName
import com.deloitte.opticrewandroid.mdui.EntitySetListActivity
import com.deloitte.opticrewandroid.mdui.InterfacedFragment
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyWorkOrderHeadersType
import com.sap.cloud.mobile.fiori.`object`.ObjectCell
import com.sap.cloud.mobile.fiori.`object`.ObjectHeader
import com.sap.cloud.mobile.odata.EntityValue
import kotlinx.android.synthetic.main.element_entityitem_list.view.*
import org.slf4j.LoggerFactory

/**
 * An activity representing a list of MyWorkOrderHeadersType. This activity has different presentations for handset and tablet-size
 * devices. On handsets, the activity presents a list of items, which when touched, lead to a view representing
 * MyWorkOrderHeadersType details. On tablets, the activity presents the list of MyWorkOrderHeadersType and MyWorkOrderHeadersType details side-by-side using two
 * vertical panes.
 */

class MyWorkOrderHeadersListFragment : InterfacedFragment<MyWorkOrderHeadersType>() {

    /**
     * Service manager to provide root URL of OData Service for Glide to load images if there are media resources
     * associated with the entity type
     */
    private var sapServiceManager: SAPServiceManager? = null

    /**
     * List adapter to be used with RecyclerView containing all instances of myWorkOrderHeaders
     */
    private var adapter: MyWorkOrderHeadersTypeListAdapter? = null

    private lateinit var refreshLayout: SwipeRefreshLayout
    private var actionMode: ActionMode? = null
    private var isInActionMode: Boolean = false
    private val selectedItems = ArrayList<Int>()

    /**
     * View model of the entity type
     */
    private lateinit var viewModel: MyWorkOrderHeadersTypeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityTitle = getString(EntitySetListActivity.EntitySetName.MyWorkOrderHeaders.titleId)
        menu = R.menu.itemlist_menu
        setHasOptionsMenu(true)
        savedInstanceState?.let {
            isInActionMode = it.getBoolean("ActionMode")
        }

        sapServiceManager = (currentActivity.application as SAPWizardApplication).sapServiceManager
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        currentActivity.findViewById<ObjectHeader>(R.id.objectHeader)?.let {
            it.visibility = View.GONE
        }
        return inflater.inflate(R.layout.fragment_entityitem_list, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(this.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_refresh -> {
                refreshLayout.isRefreshing = true
                refreshListData()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean("ActionMode", isInActionMode)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        currentActivity.setTitle(activityTitle)

        (currentActivity.findViewById<RecyclerView>(R.id.item_list))?.let {
            this.adapter = MyWorkOrderHeadersTypeListAdapter(currentActivity, it)
            it.adapter = this.adapter
        } ?: throw AssertionError()

        setupRefreshLayout()
        refreshLayout.isRefreshing = true

        navigationPropertyName = currentActivity.intent.getStringExtra("navigation")
        parentEntityData = currentActivity.intent.getParcelableExtra("parent")

        (currentActivity.findViewById<FloatingActionButton>(R.id.fab))?.let {
            if (navigationPropertyName != null && parentEntityData != null) {
                it.hide()
            } else {
                it.setOnClickListener {
                    listener?.onFragmentStateChange(UIConstants.EVENT_CREATE_NEW_ITEM, null)
                }
            }
        }

        sapServiceManager?.openODataStore {
            prepareViewModel()
        }
    }

    override fun onResume() {
        super.onResume()
        refreshListData()
    }

    /** Initializes the view model and add observers on it */
    private fun prepareViewModel() {
        viewModel = if( navigationPropertyName != null && parentEntityData != null ) {
            ViewModelProvider(currentActivity, EntityViewModelFactory(currentActivity.application, navigationPropertyName!!, parentEntityData!!))
                .get(MyWorkOrderHeadersTypeViewModel::class.java)
        } else {
            ViewModelProvider(currentActivity).get(MyWorkOrderHeadersTypeViewModel::class.java).also {
                it.initialRead{errorMessage ->
                    showError(errorMessage)
                }
            }
        }
        viewModel.observableItems.observe(viewLifecycleOwner, Observer<List<MyWorkOrderHeadersType>> { items ->
            items?.let { entityList ->
                adapter?.let { listAdapter ->
                    listAdapter.setItems(entityList)

                    var item = viewModel.selectedEntity.value?.let { containsItem(entityList, it) }
                    if (item == null) {
                        item = if (entityList.isEmpty()) null else entityList[0]
                    }

                    item?.let {
                        viewModel.inFocusId = listAdapter.getItemIdForMyWorkOrderHeadersType(it)
                        if (currentActivity.resources.getBoolean(R.bool.two_pane)) {
                            viewModel.setSelectedEntity(it)
                            if(!isInActionMode && !(currentActivity as MyWorkOrderHeadersActivity).isNavigationDisabled) {
                                listener?.onFragmentStateChange(UIConstants.EVENT_ITEM_CLICKED, it)
                            }
                        }
                        listAdapter.notifyDataSetChanged()
                    }

                    if( item == null ) hideDetailFragment()
                }

                refreshLayout.isRefreshing = false
            }
        })

        viewModel.readResult.observe(this, Observer {
            if (refreshLayout.isRefreshing) {
                refreshLayout.isRefreshing = false
            }
        })

        viewModel.deleteResult.observe( this, Observer {
            this.onDeleteComplete(it!!)
        })
    }

    /**
     * Checks if [item] exists in the list [items] based on the item id, which in offline is the read readLink,
     * while for online the primary key.
     */
    private fun containsItem(items: List<MyWorkOrderHeadersType>, item: MyWorkOrderHeadersType) : MyWorkOrderHeadersType? {
        return items.find { entry ->
            adapter?.getItemIdForMyWorkOrderHeadersType(entry) == adapter?.getItemIdForMyWorkOrderHeadersType(item)
        }
    }

    /** when no items return from server, hide the detail fragment on tablet */
    private fun hideDetailFragment() {
        currentActivity.supportFragmentManager.findFragmentByTag(UIConstants.DETAIL_FRAGMENT_TAG)?.let {
            currentActivity.supportFragmentManager.beginTransaction()
                .remove(it).commit()
        }
        secondaryToolbar?.let {
            it.menu.clear()
            it.setTitle("")
        }
        currentActivity.findViewById<ObjectHeader>(R.id.objectHeader)?.let {
            it.visibility = View.GONE
        }
    }

    /** Completion callback for delete operation  */
    private fun onDeleteComplete(result: OperationResult<MyWorkOrderHeadersType>) {
        progressBar?.let {
            it.visibility = View.INVISIBLE
        }
        viewModel.removeAllSelected()
        actionMode?.let {
            it.finish()
            isInActionMode = false
        }

        result.error?.let {
            handleDeleteError()
            return
        }
        refreshListData()
    }

    /** Handles the deletion error */
    private fun handleDeleteError() {
        showError(resources.getString(R.string.delete_failed_detail))
        refreshLayout.isRefreshing = false
    }

    /** sets up the refresh layout */
    private fun setupRefreshLayout() {
        refreshLayout = currentActivity.findViewById(R.id.swiperefresh)
        refreshLayout.setColorSchemeColors(UIConstants.FIORI_STANDARD_THEME_GLOBAL_DARK_BASE)
        refreshLayout.setProgressBackgroundColorSchemeColor(UIConstants.FIORI_STANDARD_THEME_BACKGROUND)
        refreshLayout.setOnRefreshListener(this::refreshListData)
    }

    /** Refreshes the list data */
    internal fun refreshListData() {
        navigationPropertyName?.let { _navigationPropertyName ->
            parentEntityData?.let { _parentEntityData ->
                viewModel.refresh(_parentEntityData as EntityValue, _navigationPropertyName)
            }
        } ?: run {
            viewModel.refresh()
        }
        adapter?.notifyDataSetChanged()
    }

    /** Sets the id for the selected item into view model */
    private fun setItemIdSelected(itemId: Int): MyWorkOrderHeadersType? {
        viewModel.observableItems.value?.let { myWorkOrderHeaders ->
            if (myWorkOrderHeaders.isNotEmpty()) {
                adapter?.let {
                    viewModel.inFocusId = it.getItemIdForMyWorkOrderHeadersType(myWorkOrderHeaders[itemId])
                    return myWorkOrderHeaders[itemId]
                }
            }
        }
        return null
    }

    /** Sets the detail image for the given [viewHolder] */
    private fun setDetailImage(viewHolder: MyWorkOrderHeadersTypeListAdapter.ViewHolder, myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType?) {
        if (isInActionMode) {
            val drawable: Int = if (viewHolder.isSelected) {
                R.drawable.ic_check_circle_black_24dp
            } else {
                R.drawable.ic_uncheck_circle_black_24dp
            }
            viewHolder.objectCell.prepareDetailImageView().scaleType = ImageView.ScaleType.FIT_CENTER
            Glide.with(currentActivity)
                .load(resources.getDrawable(drawable, null))
                .apply(RequestOptions().fitCenter())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(viewHolder.objectCell.prepareDetailImageView())
        } else if (!viewHolder.masterPropertyValue.isNullOrEmpty()) {
            viewHolder.objectCell.detailImageCharacter = viewHolder.masterPropertyValue?.substring(0, 1)
        } else {
            viewHolder.objectCell.detailImageCharacter = "?"
        }
    }

    /**
     * Represents the listener to start the action mode. 
     */
    inner class OnActionModeStartClickListener(internal var holder: MyWorkOrderHeadersTypeListAdapter.ViewHolder) : View.OnClickListener, View.OnLongClickListener {

        override fun onClick(view: View) {
            onAnyKindOfClick()
        }

        override fun onLongClick(view: View): Boolean {
            return onAnyKindOfClick()
        }

        /** callback function for both normal and long click of an entity */
        private fun onAnyKindOfClick(): Boolean {
            val isNavigationDisabled = (activity as MyWorkOrderHeadersActivity).isNavigationDisabled
            if (isNavigationDisabled) {
                Toast.makeText(activity, "Please save your changes first...", Toast.LENGTH_LONG).show()
            } else {
                if (!isInActionMode) {
                    actionMode = (currentActivity as AppCompatActivity).startSupportActionMode(MyWorkOrderHeadersListActionMode())
                    adapter?.notifyDataSetChanged()
                }
                holder.isSelected = !holder.isSelected
            }
            return true
        }
    }

    /**
     * Represents list action mode.
     */
    inner class MyWorkOrderHeadersListActionMode : ActionMode.Callback {
        override fun onCreateActionMode(actionMode: ActionMode, menu: Menu): Boolean {
            isInActionMode = true
            (currentActivity.findViewById<FloatingActionButton>(R.id.fab))?.let {
                it.hide()
            }
            //(currentActivity as MyWorkOrderHeadersActivity).onSetActionModeFlag(isInActionMode)
            val inflater = actionMode.menuInflater
            inflater.inflate(R.menu.itemlist_view_options, menu)

            hideDetailFragment()
            return true
        }

        override fun onPrepareActionMode(actionMode: ActionMode, menu: Menu): Boolean {
            return false
        }

        override fun onActionItemClicked(actionMode: ActionMode, menuItem: MenuItem): Boolean {
            return when (menuItem.itemId) {
                R.id.update_item -> {
                    val myWorkOrderHeadersTypeEntity = viewModel.getSelected(0)
                    if (viewModel.numberOfSelected() == 1 && myWorkOrderHeadersTypeEntity != null) {
                        isInActionMode = false
                        actionMode.finish()
                        viewModel.setSelectedEntity(myWorkOrderHeadersTypeEntity)
                        if(currentActivity.resources.getBoolean(R.bool.two_pane)) {
                            //make sure 'view' is under 'crt/update',
                            //so after done or back, the right panel has things to view
                            listener?.onFragmentStateChange(UIConstants.EVENT_ITEM_CLICKED, myWorkOrderHeadersTypeEntity)
                        }
                        listener?.onFragmentStateChange(UIConstants.EVENT_EDIT_ITEM, myWorkOrderHeadersTypeEntity)
                    }
                    true
                }
                R.id.delete_item -> {
                    listener?.onFragmentStateChange(UIConstants.EVENT_ASK_DELETE_CONFIRMATION,null)
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(actionMode: ActionMode) {
            isInActionMode = false
            if (!(navigationPropertyName != null && parentEntityData != null)) {
                (currentActivity.findViewById<FloatingActionButton>(R.id.fab))?.let {
                    it.show()
                }
            }
            selectedItems.clear()
            viewModel.removeAllSelected()

            //if in big screen, make sure one item is selected.
            refreshListData()
        }
    }

    /**
    * List adapter to be used with RecyclerView. It contains the set of myWorkOrderHeaders.
    */
    inner class MyWorkOrderHeadersTypeListAdapter(private val context: Context, private val recyclerView: RecyclerView) : RecyclerView.Adapter<MyWorkOrderHeadersTypeListAdapter.ViewHolder>() {

        /** Entire list of MyWorkOrderHeadersType collection */
        private var myWorkOrderHeaders: MutableList<MyWorkOrderHeadersType> = ArrayList()

        /** Flag to indicate whether we have checked retained selected myWorkOrderHeaders */
        private var checkForSelectedOnCreate = false

        init {
            setHasStableIds(true)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyWorkOrderHeadersTypeListAdapter.ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.element_entityitem_list, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return myWorkOrderHeaders.size
        }

        override fun getItemId(position: Int): Long {
            return getItemIdForMyWorkOrderHeadersType(myWorkOrderHeaders[position])
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            checkForRetainedSelection()

            val myWorkOrderHeadersTypeEntity = myWorkOrderHeaders[holder.adapterPosition]
            (myWorkOrderHeadersTypeEntity.getDataValue(MyWorkOrderHeadersType.orderDescription))?.let {
                holder.masterPropertyValue = it.toString()
            }
            populateObjectCell(holder, myWorkOrderHeadersTypeEntity)

            val isActive = getItemIdForMyWorkOrderHeadersType(myWorkOrderHeadersTypeEntity) == viewModel.inFocusId
            if (isActive) {
                setItemIdSelected(holder.adapterPosition)
            }
            val isMyWorkOrderHeadersTypeSelected = viewModel.selectedContains(myWorkOrderHeadersTypeEntity)
            setViewBackground(holder.objectCell, isMyWorkOrderHeadersTypeSelected, isActive)

            holder.view.setOnLongClickListener(OnActionModeStartClickListener(holder))
            setOnClickListener(holder, myWorkOrderHeadersTypeEntity)

            setOnCheckedChangeListener(holder, myWorkOrderHeadersTypeEntity)
            holder.isSelected = isMyWorkOrderHeadersTypeSelected
            setDetailImage(holder, myWorkOrderHeadersTypeEntity)
        }

        /**
        * Check to see if there are an retained selected myWorkOrderHeadersTypeEntity on start.
        * This situation occurs when a rotation with selected myWorkOrderHeaders is triggered by user.
        */
        private fun checkForRetainedSelection() {
            if (!checkForSelectedOnCreate) {
                checkForSelectedOnCreate = true
                if (viewModel.numberOfSelected() > 0) {
                    manageActionModeOnCheckedTransition()
                }
            }
        }

        /**
        * Computes a stable ID for each MyWorkOrderHeadersType object for use to locate the ViewHolder
        *
        * @param [myWorkOrderHeadersTypeEntity] to get the items for
        * @return an ID based on the primary key of MyWorkOrderHeadersType
        */
        internal fun getItemIdForMyWorkOrderHeadersType(myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType): Long {
            return myWorkOrderHeadersTypeEntity.entityKey.toString().hashCode().toLong()
        }

        /**
        * Start Action Mode if it has not been started
        *
        * This is only called when long press action results in a selection. Hence action mode may not have been
        * started. Along with starting action mode, title will be set. If this is an additional selection, adjust title
        * appropriately.
        */
        private fun manageActionModeOnCheckedTransition() {
            if (actionMode == null) {
                actionMode = (activity as AppCompatActivity).startSupportActionMode(MyWorkOrderHeadersListActionMode())
            }
            if (viewModel.numberOfSelected() > 1) {
                actionMode?.menu?.findItem(R.id.update_item)?.isVisible = false
            }
            actionMode?.title = viewModel.numberOfSelected().toString()
        }

        /**
        * This is called when one of the selected myWorkOrderHeaders has been de-selected
        *
        * On this event, we will determine if update action needs to be made visible or action mode should be
        * terminated (no more selected)
        */
        private fun manageActionModeOnUncheckedTransition() {
            when (viewModel.numberOfSelected()) {
                1 -> actionMode?.menu?.findItem(R.id.update_item)?.isVisible = true
                0 -> {
                    actionMode?.finish()
                    actionMode = null
                    return
                }
            }
            actionMode?.title = viewModel.numberOfSelected().toString()
        }

        private fun populateObjectCell(viewHolder: ViewHolder, myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType) {

            val dataValue = myWorkOrderHeadersTypeEntity.getDataValue(MyWorkOrderHeadersType.orderDescription)
            var masterPropertyValue: String? = null
            if (dataValue != null) {
                masterPropertyValue = dataValue.toString()
            }
            viewHolder.objectCell.apply {
                headline = masterPropertyValue
                detailImage = null
                subheadline = "Subheadline goes here"
                footnote = "Footnote goes here"
                if (masterPropertyValue == null || masterPropertyValue.isEmpty()) {
                setIcon("?", 0)
                } else {
                setIcon(masterPropertyValue.substring(0, 1), 0)
                }
                setIcon(R.drawable.default_dot, 1, R.string.attachment_item_content_desc)
                setIcon("!", 2)
            }
        }

        private fun processClickAction(viewHolder: ViewHolder, myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType) {
            resetPreviouslyClicked()
            setViewBackground(viewHolder.objectCell, false, true)
            viewModel.inFocusId = getItemIdForMyWorkOrderHeadersType(myWorkOrderHeadersTypeEntity)
        }

        /**
        * Attempt to locate previously clicked view and reset its background
        * Reset view model's inFocusId
        */
        private fun resetPreviouslyClicked() {
            (recyclerView.findViewHolderForItemId(viewModel.inFocusId) as ViewHolder?)?.let {
                setViewBackground(it.objectCell, it.isSelected, false)
            } ?: run {
                viewModel.refresh()
            }
        }

        /**
        * If there are selected myWorkOrderHeaders via long press, clear them as click and long press are mutually exclusive
        * In addition, since we are clearing all selected myWorkOrderHeaders via long press, finish the action mode.
        */
        private fun resetSelected() {
            if (viewModel.numberOfSelected() > 0) {
                viewModel.removeAllSelected()
                if (actionMode != null) {
                    actionMode?.finish()
                    actionMode = null
                }
            }
        }

        /**
        * Set up checkbox value and visibility based on myWorkOrderHeadersTypeEntity selection status
        *
        * @param [checkBox] to set
        * @param [isMyWorkOrderHeadersTypeSelected] true if myWorkOrderHeadersTypeEntity is selected via long press action
        */
        private fun setCheckBox(checkBox: CheckBox, isMyWorkOrderHeadersTypeSelected: Boolean) {
            checkBox.isChecked = isMyWorkOrderHeadersTypeSelected
        }

        /**
        * Use DiffUtil to calculate the difference and dispatch them to the adapter
        * Note: Please use background thread for calculation if the list is large to avoid blocking main thread
        */
        @WorkerThread
        fun setItems(currentMyWorkOrderHeaders: List<MyWorkOrderHeadersType>) {
            if (myWorkOrderHeaders.isEmpty()) {
                myWorkOrderHeaders = java.util.ArrayList(currentMyWorkOrderHeaders)
                notifyItemRangeInserted(0, currentMyWorkOrderHeaders.size)
            } else {
                val result = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                    override fun getOldListSize(): Int {
                        return myWorkOrderHeaders.size
                    }

                    override fun getNewListSize(): Int {
                        return currentMyWorkOrderHeaders.size
                    }

                    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                        return myWorkOrderHeaders[oldItemPosition].entityKey.toString() == currentMyWorkOrderHeaders[newItemPosition].entityKey.toString()
                    }

                    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                        val myWorkOrderHeadersTypeEntity = myWorkOrderHeaders[oldItemPosition]
                        return !myWorkOrderHeadersTypeEntity.isUpdated && currentMyWorkOrderHeaders[newItemPosition] == myWorkOrderHeadersTypeEntity
                    }
                })
                myWorkOrderHeaders.clear()
                myWorkOrderHeaders.addAll(currentMyWorkOrderHeaders)
                result.dispatchUpdatesTo(this)
            }
        }

        /**
        * Set ViewHolder's CheckBox onCheckedChangeListener
        *
        * @param [holder] to set
        * @param [myWorkOrderHeadersTypeEntity] associated with this ViewHolder
        */
        private fun setOnCheckedChangeListener(holder: ViewHolder, myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType) {
            holder.checkBox.setOnCheckedChangeListener { _, checked ->
                if (checked) {
                    //(currentActivity as MyWorkOrderHeadersActivity).onUnderDeletion(myWorkOrderHeadersTypeEntity, true)
                    viewModel.addSelected(myWorkOrderHeadersTypeEntity)
                    manageActionModeOnCheckedTransition()
                    resetPreviouslyClicked()
                } else {
                    //(currentActivity as MyWorkOrderHeadersActivity).onUnderDeletion(myWorkOrderHeadersTypeEntity, false)
                    viewModel.removeSelected(myWorkOrderHeadersTypeEntity)
                    manageActionModeOnUncheckedTransition()
                }
                setViewBackground(holder.objectCell, viewModel.selectedContains(myWorkOrderHeadersTypeEntity), false)
                setDetailImage(holder, myWorkOrderHeadersTypeEntity)
            }
        }

        /**
        * Set ViewHolder's view onClickListener
        *
        * @param [holder] to set
        * @param [myWorkOrderHeadersTypeEntity] associated with this ViewHolder
        */
        private fun setOnClickListener(holder: ViewHolder, myWorkOrderHeadersTypeEntity: MyWorkOrderHeadersType) {
            holder.view.setOnClickListener { view ->
                val isNavigationDisabled = (currentActivity as MyWorkOrderHeadersActivity).isNavigationDisabled
                if( !isNavigationDisabled ) {
                    resetSelected()
                    resetPreviouslyClicked()
                    processClickAction(holder, myWorkOrderHeadersTypeEntity)
                    viewModel.setSelectedEntity(myWorkOrderHeadersTypeEntity)
                    listener?.onFragmentStateChange(UIConstants.EVENT_ITEM_CLICKED, myWorkOrderHeadersTypeEntity)
                } else {
                    Toast.makeText(currentActivity, "Please save your changes first...", Toast.LENGTH_LONG).show()
                }
            }
        }

        /**
        * Set background of view to indicate myWorkOrderHeadersTypeEntity selection status
        * Selected and Active are mutually exclusive. Only one can be true
        *
        * @param [view]
        * @param [isMyWorkOrderHeadersTypeSelected] - true if myWorkOrderHeadersTypeEntity is selected via long press action
        * @param [isActive]           - true if myWorkOrderHeadersTypeEntity is selected via click action
        */
        private fun setViewBackground(view: View, isMyWorkOrderHeadersTypeSelected: Boolean, isActive: Boolean) {
            val isMasterDetailView = currentActivity.resources.getBoolean(R.bool.two_pane)
            if (isMyWorkOrderHeadersTypeSelected) {
                view.background = ContextCompat.getDrawable(context, R.drawable.list_item_selected)
            } else if (isActive && isMasterDetailView && !isInActionMode) {
                view.background = ContextCompat.getDrawable(context, R.drawable.list_item_active)
            } else {
                view.background = ContextCompat.getDrawable(context, R.drawable.list_item_default)
            }
        }

        /**
        * ViewHolder for RecyclerView.
        * Each view has a Fiori ObjectCell and a checkbox (used by long press)
        */
        inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

            var isSelected = false
                set(selected) {
                    field = selected
                    checkBox.isChecked = selected
                }

            var masterPropertyValue: String? = null

            /** Fiori ObjectCell to display myWorkOrderHeadersTypeEntity in list */
            val objectCell: ObjectCell = view.content

            /** Checkbox for long press selection */
            val checkBox: CheckBox = view.cbx

            override fun toString(): String {
                return super.toString() + " '" + objectCell.description + "'"
            }
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(MyWorkOrderHeadersActivity::class.java)
    }
}
