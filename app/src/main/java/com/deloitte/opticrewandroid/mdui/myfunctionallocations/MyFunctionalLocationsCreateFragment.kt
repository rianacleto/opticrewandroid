package com.deloitte.opticrewandroid.mdui.myfunctionallocations

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.databinding.FragmentMyfunctionallocationsCreateBinding
import com.deloitte.opticrewandroid.mdui.BundleKeys
import com.deloitte.opticrewandroid.mdui.InterfacedFragment
import com.deloitte.opticrewandroid.mdui.UIConstants
import com.deloitte.opticrewandroid.repository.OperationResult
import com.deloitte.opticrewandroid.viewmodel.myfunctionallocationstype.MyFunctionalLocationsTypeViewModel
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyFunctionalLocationsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntityTypes
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets
import com.sap.cloud.mobile.fiori.formcell.SimplePropertyFormCell
import com.sap.cloud.mobile.fiori.`object`.ObjectHeader
import com.sap.cloud.mobile.odata.Property
import org.slf4j.LoggerFactory

/**
 * A fragment that is used for both update and create for users to enter values for the properties. When used for
 * update, an instance of the entity is required. In the case of create, a new instance of the entity with defaults will
 * be created. The default values may not be acceptable for the OData service.
 * This fragment is either contained in a [MyFunctionalLocationsListActivity] in two-pane mode (on tablets) or a
 * [MyFunctionalLocationsDetailActivity] on handsets.
 *
 * Arguments: Operation: [OP_CREATE | OP_UPDATE]
 *            MyFunctionalLocationsType if Operation is update
 */
class MyFunctionalLocationsCreateFragment : InterfacedFragment<MyFunctionalLocationsType>() {

    /** MyFunctionalLocationsType object and it's copy: the modifications are done on the copied object. */
    private lateinit var myFunctionalLocationsTypeEntity: MyFunctionalLocationsType
    private lateinit var myFunctionalLocationsTypeEntityCopy: MyFunctionalLocationsType

    /** DataBinding generated class */
    private lateinit var binding: FragmentMyfunctionallocationsCreateBinding

    /** Indicate what operation to be performed */
    private lateinit var operation: String

    /** myFunctionalLocationsTypeEntity ViewModel */
    private lateinit var viewModel: MyFunctionalLocationsTypeViewModel

    /** The update menu item */
    private lateinit var updateMenuItem: MenuItem

    private val isMyFunctionalLocationsTypeValid: Boolean
        get() {
            var isValid = true
            view?.findViewById<LinearLayout>(R.id.create_update_myfunctionallocationstype)?.let { linearLayout ->
                for (i in 0 until linearLayout.childCount) {
                    val simplePropertyFormCell = linearLayout.getChildAt(i) as SimplePropertyFormCell
                    val propertyName = simplePropertyFormCell.tag as String
                    val property = EntityTypes.myFunctionalLocationsType.getProperty(propertyName)
                    val value = simplePropertyFormCell.value.toString()
                    if (!isValidProperty(property, value)) {
                        simplePropertyFormCell.setTag(R.id.TAG_HAS_MANDATORY_ERROR, true)
                        val errorMessage = resources.getString(R.string.mandatory_warning)
                        simplePropertyFormCell.isErrorEnabled = true
                        simplePropertyFormCell.error = errorMessage
                        isValid = false
                    } else {
                        if (simplePropertyFormCell.isErrorEnabled) {
                            val hasMandatoryError = simplePropertyFormCell.getTag(R.id.TAG_HAS_MANDATORY_ERROR) as Boolean
                            if (!hasMandatoryError) {
                                isValid = false
                            } else {
                                simplePropertyFormCell.isErrorEnabled = false
                            }
                        }
                        simplePropertyFormCell.setTag(R.id.TAG_HAS_MANDATORY_ERROR, false)
                    }
                }
            }
            return isValid
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        menu = R.menu.itemlist_edit_options
        setHasOptionsMenu(true)

        arguments?.let {
            (it.getString(BundleKeys.OPERATION))?.let { operationType ->
                operation = operationType
                activityTitle = when (operationType) {
                    UIConstants.OP_CREATE -> resources.getString(R.string.title_create_fragment, EntityTypes.myFunctionalLocationsType.localName)
                    else -> resources.getString(R.string.title_update_fragment) + " " + EntityTypes.myFunctionalLocationsType.localName

                }
            }
        }

        activity?.let {
            (it as MyFunctionalLocationsActivity).isNavigationDisabled = true
            viewModel = ViewModelProvider(it).get(MyFunctionalLocationsTypeViewModel::class.java)
            viewModel.createResult.observe(this, Observer { result -> onComplete(result!!) })
            viewModel.updateResult.observe(this, Observer { result -> onComplete(result!!) })

            if (operation == UIConstants.OP_CREATE) {
                myFunctionalLocationsTypeEntity = createMyFunctionalLocationsType()
            } else {
                myFunctionalLocationsTypeEntity = viewModel.selectedEntity.value!!
            }

            val workingCopy = savedInstanceState?.getParcelable<MyFunctionalLocationsType>(KEY_WORKING_COPY)
            if (workingCopy == null) {
                myFunctionalLocationsTypeEntityCopy = myFunctionalLocationsTypeEntity.copy()
                myFunctionalLocationsTypeEntityCopy.setEntityTag(myFunctionalLocationsTypeEntity.getEntityTag())
                myFunctionalLocationsTypeEntityCopy.setOldEntity(myFunctionalLocationsTypeEntity)
                myFunctionalLocationsTypeEntityCopy.editLink = myFunctionalLocationsTypeEntity.editLink
            } else {
                myFunctionalLocationsTypeEntityCopy = workingCopy
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        currentActivity.findViewById<ObjectHeader>(R.id.objectHeader)?.let {
            it.visibility = View.GONE
        }
        val rootView = setupDataBinding(inflater, container)
        return rootView
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.save_item -> {
                updateMenuItem = item
                enableUpdateMenuItem(false)
                onSaveItem()
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if(secondaryToolbar != null) secondaryToolbar!!.setTitle(activityTitle) else activity?.setTitle(activityTitle)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(KEY_WORKING_COPY, myFunctionalLocationsTypeEntityCopy)
        super.onSaveInstanceState(outState)
    }

    /** Enables the update menu item based on [enable] */
    private fun enableUpdateMenuItem(enable : Boolean = true) {
        updateMenuItem.also {
            it.isEnabled = enable
            it.icon.alpha = if(enable) 255 else 130
        }
    }

    /** Saves the entity */
    private fun onSaveItem(): Boolean {
        if (!isMyFunctionalLocationsTypeValid) {
            return false
        }
        (currentActivity as MyFunctionalLocationsActivity).isNavigationDisabled = false
        progressBar?.visibility = View.VISIBLE
        when (operation) {
            UIConstants.OP_CREATE -> {
                viewModel.create(myFunctionalLocationsTypeEntityCopy)
            }
            UIConstants.OP_UPDATE -> viewModel.update(myFunctionalLocationsTypeEntityCopy)
        }
        return true
    }

    /**
     * Create a new MyFunctionalLocationsType instance and initialize properties to its default values
     * Nullable property will remain null
     * @return new MyFunctionalLocationsType instance
     */
    private fun createMyFunctionalLocationsType(): MyFunctionalLocationsType {
        val entity = MyFunctionalLocationsType(true)
        return entity
    }

    /** Callback function to complete processing when updateResult or createResult events fired */
    private fun onComplete(result: OperationResult<MyFunctionalLocationsType>) {
        progressBar?.visibility = View.INVISIBLE
        enableUpdateMenuItem(true)
        if (result.error != null) {
            (currentActivity as MyFunctionalLocationsActivity).isNavigationDisabled = true
            handleError(result)
        } else {
            if (operation == UIConstants.OP_UPDATE && !currentActivity.resources.getBoolean(R.bool.two_pane)) {
                viewModel.selectedEntity.value = myFunctionalLocationsTypeEntityCopy
            }
            (currentActivity as MyFunctionalLocationsActivity).onBackPressed()
        }
    }

    /** Simple validation: checks the presence of mandatory fields. */
    private fun isValidProperty(property: Property, value: String): Boolean {
        return !(!property.isNullable && value.isEmpty())
    }

    /**
     * Set up data binding for this view
     *
     * @param [inflater] layout inflater from onCreateView
     * @param [container] view group from onCreateView
     *
     * @return rootView from generated data binding code
     */
    private fun setupDataBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentMyfunctionallocationsCreateBinding.inflate(inflater, container, false)
        binding.setMyFunctionalLocationsType(myFunctionalLocationsTypeEntityCopy)
        return binding.root
    }

    /**
     * Notify user of error encountered while execution the operation
     *
     * @param [result] operation result with error
     */
    private fun handleError(result: OperationResult<MyFunctionalLocationsType>) {
        val errorMessage = when (result.operation) {
            OperationResult.Operation.UPDATE -> getString(R.string.update_failed_detail)
            OperationResult.Operation.CREATE -> getString(R.string.create_failed_detail)
            else -> throw AssertionError()
        }
        showError(errorMessage)
    }


    companion object {
        private val KEY_WORKING_COPY = "WORKING_COPY"
        private val LOGGER = LoggerFactory.getLogger(MyFunctionalLocationsActivity::class.java)
    }
}
