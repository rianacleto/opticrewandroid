package com.deloitte.opticrewandroid.repository
import com.deloitte.opticrewandroid.service.SAPServiceManager

import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.cds_zsd_sam_odata_EntitiesMetadata.EntitySets
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.EmployeesType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MaterialsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyEquipmentsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyFunctionalLocationsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyWorkCentersType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyWorkOrderHeadersType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.MyWorkOrderOperationsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationComponentsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCentersType

import com.sap.cloud.mobile.odata.EntitySet
import com.sap.cloud.mobile.odata.EntityValue
import com.sap.cloud.mobile.odata.Property

import java.util.WeakHashMap

/*
 * Repository factory to construct repository for an entity set
 */
class RepositoryFactory
/**
 * Construct a RepositoryFactory instance. There should only be one repository factory and used
 * throughout the life of the application to avoid caching entities multiple times.
 * @param sapServiceManager - Service manager for interaction with OData service
 */
(private val sapServiceManager: SAPServiceManager?) {
    private val repositories: WeakHashMap<String, Repository<out EntityValue>> = WeakHashMap()

    /**
     * Construct or return an existing repository for the specified entity set
     * @param entitySet - entity set for which the repository is to be returned
     * @param orderByProperty - if specified, collection will be sorted ascending with this property
     * @return a repository for the entity set
     */
    fun getRepository(entitySet: EntitySet, orderByProperty: Property?): Repository<out EntityValue> {
        val cds_zsd_sam_odata_Entities = sapServiceManager?.cds_zsd_sam_odata_Entities
        val key = entitySet.localName
        var repository: Repository<out EntityValue>? = repositories[key]
        if (repository == null) {
            repository = when (key) {
                EntitySets.employees.localName -> Repository<EmployeesType>(cds_zsd_sam_odata_Entities!!, EntitySets.employees, orderByProperty)
                EntitySets.materials.localName -> Repository<MaterialsType>(cds_zsd_sam_odata_Entities!!, EntitySets.materials, orderByProperty)
                EntitySets.myEquipments.localName -> Repository<MyEquipmentsType>(cds_zsd_sam_odata_Entities!!, EntitySets.myEquipments, orderByProperty)
                EntitySets.myFunctionalLocations.localName -> Repository<MyFunctionalLocationsType>(cds_zsd_sam_odata_Entities!!, EntitySets.myFunctionalLocations, orderByProperty)
                EntitySets.myWorkCenters.localName -> Repository<MyWorkCentersType>(cds_zsd_sam_odata_Entities!!, EntitySets.myWorkCenters, orderByProperty)
                EntitySets.myWorkOrderHeaders.localName -> Repository<MyWorkOrderHeadersType>(cds_zsd_sam_odata_Entities!!, EntitySets.myWorkOrderHeaders, orderByProperty)
                EntitySets.myWorkOrderOperations.localName -> Repository<MyWorkOrderOperationsType>(cds_zsd_sam_odata_Entities!!, EntitySets.myWorkOrderOperations, orderByProperty)
                EntitySets.operationComponents.localName -> Repository<OperationComponentsType>(cds_zsd_sam_odata_Entities!!, EntitySets.operationComponents, orderByProperty)
                EntitySets.operations.localName -> Repository<OperationsType>(cds_zsd_sam_odata_Entities!!, EntitySets.operations, orderByProperty)
                EntitySets.workCenterEmployees.localName -> Repository<WorkCenterEmployeesType>(cds_zsd_sam_odata_Entities!!, EntitySets.workCenterEmployees, orderByProperty)
                EntitySets.workCenters.localName -> Repository<WorkCentersType>(cds_zsd_sam_odata_Entities!!, EntitySets.workCenters, orderByProperty)
                else -> throw AssertionError("Fatal error, entity set[$key] missing in generated code")
            }
            repositories[key] = repository
        }
        return repository
    }

    /**
     * Get rid of all cached repositories
     */
    fun reset() {
        repositories.clear()
    }
}
