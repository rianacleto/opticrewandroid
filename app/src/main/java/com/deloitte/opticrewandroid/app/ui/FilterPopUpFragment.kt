package com.deloitte.opticrewandroid.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.deloitte.opticrewandroid.R
import kotlinx.android.synthetic.main.tasks_filter_layout.*
import kotlinx.android.synthetic.main.tasks_filter_layout.view.*

class FilterPopUpFragment(
    private val onApplyAction: (List<String>) -> Unit
) : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.isCancelable = false
        val rootView = inflater.inflate(R.layout.tasks_filter_layout, container, false)

        rootView.clearAllBtn.setOnClickListener {
            rootView.cbCasting.isChecked = false
            rootView.cbMachining.isChecked = false
            rootView.cbMolding.isChecked = false
            rootView.cbPrudoctionEngineering.isChecked = false
        }

        rootView.filterCancelBtn.setOnClickListener { dismiss() }

        rootView.applyBtn.setOnClickListener {
            onApplyAction(getSelectedFilters())
            this.dismiss()
        }

        return rootView
    }

    private fun getSelectedFilters(): List<String> {
        val list = mutableListOf<String>()
        // TODO: Check how this is supposed to work there....
        if (cbCasting.isChecked) {
            list.add(FilterList.casting.name)
        }
        if (cbMachining.isChecked) {
            list.add(FilterList.machining.name)
        }
        if (cbMolding.isChecked) {
            list.add(FilterList.molding.name)
        }
        if (cbPrudoctionEngineering.isChecked) {
            list.add(FilterList.productionEngineering.name)
        }
        return list
    }

    enum class FilterList { casting, machining, molding, productionEngineering }

    companion object{
        const val FILTER_TAG = "com.deloitte.opticrewandroid.app.ui.FilterPopUpFragment"
    }
}