package com.deloitte.opticrewandroid.app

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.*
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.Utils
import com.deloitte.opticrewandroid.adapters.EmployeeListAdapter
import com.deloitte.opticrewandroid.adapters.HeadersListAdapter
import com.deloitte.opticrewandroid.adapters.OperationsViewsAdapter
import com.deloitte.opticrewandroid.app.ui.FilterPopUpFragment
import com.deloitte.opticrewandroid.app.ui.PersonInfoPopUpFragment
import com.deloitte.opticrewandroid.app.ui.TaskDetailsPopUpFragment
import com.deloitte.opticrewandroid.app.ui.UnassignedTasksPopUpFragment
import com.deloitte.opticrewandroid.configurations.Constants.MIN_TO_DPS_RATIO
import com.deloitte.opticrewandroid.databinding.TaskGridLayoutBinding
import com.deloitte.opticrewandroid.helpers.RecyclerItemTouchHelper
import com.deloitte.opticrewandroid.mdui.operations.OperationsDetailFragment
import com.deloitte.opticrewandroid.usecases.EmployeeToOperationsMergeUseCase
import com.deloitte.opticrewandroid.viewmodel.tasksview.TasksViewViewModel
import com.google.android.material.navigation.NavigationView
import java.text.SimpleDateFormat
import java.util.*


class TasksViewActivity : AppCompatActivity(),
    RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private val binding by lazy {
        TaskGridLayoutBinding
            .inflate(layoutInflater)
    }
    val scrollListeners = arrayOfNulls<RecyclerView.OnScrollListener>(4)

    private val viewModel by lazy {
        ViewModelProvider(
            this, TasksViewViewModel.Factory(
                this,
                application,
                this,
                EmployeeToOperationsMergeUseCase()
            )
        ).get(TasksViewViewModel::class.java)
    }

    private lateinit var workcentersAdapter: ArrayAdapter<String>
    private val headersAdapter by lazy { HeadersListAdapter(this) }
    private val operationsAdapter by lazy {
        OperationsViewsAdapter(
            { taskItem ->
                TaskDetailsPopUpFragment(
                    taskItem.operationsType
                ) { updatedOp ->

                }.show(supportFragmentManager, TaskDetailsPopUpFragment.TASK_DETAILS_TAG)
            },
            this
        )
    }
    private val employeesAdapter by lazy {
        EmployeeListAdapter(
            { employee ->
                PersonInfoPopUpFragment(
                    employee
                ) { personNumber ->
                    // TODO unassign all tasks from this person
                }.show(
                    supportFragmentManager,
                    PersonInfoPopUpFragment.PERSON_DETAILS_TAG
                )
            },
            this
        )
    }

    // region UI Elements
    private val headersRecycler by lazy {
        binding.headersRecycler
    }
    private val operationsRecycler by lazy {
        binding.operationsRecycler
    }
    private val employeesRecycler by lazy {
        binding.employeeRecycler
    }
    private val operationsContainer by lazy {
        binding.operationsRecyclerContainer
    }
    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setViewModelObserves()
        setListenersAndDrawerAdapter()
        viewModel.getWorkCenters()
        val sdf = SimpleDateFormat("EE, dd MMM, yyyy", Locale.getDefault())
        binding.dayText.setText(sdf.format(Calendar.getInstance().time))
    }

    private fun setListenersAndDrawerAdapter() {
        val navigationView = findViewById<View>(R.id.navigationDrawer) as NavigationView
        val navHeader = navigationView.getHeaderView(0)
        val autoCplt = navHeader.findViewById<AutoCompleteTextView>(R.id.autoComplete)

        autoCplt.setOnItemClickListener { adapterView, view, position, l ->
            viewModel.onWorkCenterSelected(position)
            binding.drawerLayout.closeDrawer(binding.navigationDrawer, true)
        }

        binding.drawerButton.setOnClickListener {
            binding.drawerLayout.openDrawer(GravityCompat.START, true)
        }
        binding.filterButton.setOnClickListener {
            FilterPopUpFragment() {}.show(
                supportFragmentManager,
                FilterPopUpFragment.FILTER_TAG
            )
        }
        binding.unassignTasksButton.setOnClickListener {
            UnassignedTasksPopUpFragment() {}.show(
                supportFragmentManager,
                UnassignedTasksPopUpFragment.UNASSIGNED_TASKS_POPUP_TAG
            )
        }
    }

    private fun setViewModelObserves() {
        viewModel.workCenters.observe(this,
            { items ->
                val workCenterNames = mutableListOf<String>()
                for (workCenter in items) {
                    workCenterNames.add(workCenter.workCenterName)
                }
                workcentersAdapter =
                    ArrayAdapter(this, R.layout.workcenters_dropdown_item_layout, workCenterNames)

                val navigationView = findViewById<View>(R.id.navigationDrawer) as NavigationView
                val navHeader = navigationView.getHeaderView(0)
                val autoCplt = navHeader.findViewById<AutoCompleteTextView>(R.id.autoComplete)
                autoCplt.setAdapter(workcentersAdapter)

                workcentersAdapter.notifyDataSetChanged()
            })

        viewModel.columnCount.observe(this,
            { count ->
                count?.also {
                    prepareRecyclerViews(it)
                }
            })
        viewModel.employeeAndOperationProcessingResult.observe(this,
            { items ->
                items?.also {
                    operationsAdapter.setData(it.operationsList)
                    employeesAdapter.setData(it.employeeList)
                    headersAdapter.setData(it.headerList)
                    binding.recyclerTopDivider.visibility = View.VISIBLE
                }
            })
    }

    private fun prepareRecyclerViews(spanCount: Int) {

        val touchHelper = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this)

        // region headers recycler
        val headersLayoutManager = LinearLayoutManager(
            this,
            RecyclerView.HORIZONTAL, false
        )
        headersRecycler.layoutManager = headersLayoutManager
        headersRecycler.adapter = headersAdapter
        // enregion

        // region Operations recycler
        val layoutManager = GridLayoutManager(this, spanCount)
        // Setting dynamic row length
        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return operationsAdapter.getItemAt(position).width
            }
        }
        operationsRecycler.layoutManager = layoutManager
        operationsRecycler.layoutParams = FrameLayout.LayoutParams(
            Utils.pixelsFromDps(spanCount * MIN_TO_DPS_RATIO, this),
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        operationsRecycler.adapter = operationsAdapter
        // endregion

        // region Employees recycler
        val employeeLayoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL, false
        )
        employeesRecycler.layoutManager = employeeLayoutManager
        employeesRecycler.adapter = employeesAdapter
        ItemTouchHelper(touchHelper).attachToRecyclerView(employeesRecycler)
        // enregion


        // Setting horizontal line between rows
        DividerItemDecoration(
            this,
            layoutManager.orientation
        ).apply {
            operationsRecycler.addItemDecoration(this)
            employeesRecycler.addItemDecoration(this)
        }
        syncScrolls()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun syncScrolls() {

        scrollListeners[0] = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                scrollListeners[1]?.also {
                    operationsRecycler.removeOnScrollListener(it)
                    operationsRecycler.scrollBy(dy, dy)
                    operationsRecycler.addOnScrollListener(it)

                }

            }
        }
        scrollListeners[1] = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                scrollListeners[0]?.also {
                    employeesRecycler.removeOnScrollListener(it)
                    employeesRecycler.scrollBy(dx, dy)
                    employeesRecycler.addOnScrollListener(it)
                }
                scrollListeners[2]?.also {
                    headersRecycler.removeOnScrollListener(it)
                    headersRecycler.scrollBy(dx, dy)
                    headersRecycler.addOnScrollListener(it)
                }

            }
        }

        scrollListeners[2] = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                operationsContainer.scrollBy(dx, dy)
            }
        }

        operationsContainer.setOnScrollChangeListener { p0, newX, newY, oldX, oldY ->
            scrollListeners[2]?.also {
                headersRecycler.removeOnScrollListener(it)
                headersRecycler.scrollBy(newX - oldX, newY - oldY)
                headersRecycler.addOnScrollListener(it)
            }

        }


        scrollListeners[0]?.also {
            employeesRecycler.addOnScrollListener(it)
        }
        scrollListeners[1]?.also {
            operationsRecycler.addOnScrollListener(it)
        }
        scrollListeners[2]?.also {
            headersRecycler.addOnScrollListener(it)
        }

    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int, position: Int) {
        // TODO Unassign tasks
        employeesAdapter.notifyDataSetChanged()
    }
}

