package com.deloitte.opticrewandroid.app.ui

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.TimePicker
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.deloitte.opticrewandroid.R
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import kotlinx.android.synthetic.main.task_details_fragment_layout.view.*
import java.text.SimpleDateFormat
import java.util.*

class TaskDetailsPopUpFragment(
    private val operation: OperationsType?,
    private val onSaveAction: (operation: OperationsType) -> Unit
) : DialogFragment() {
    private var rootView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.isCancelable = false
        rootView = inflater.inflate(
            R.layout.task_details_fragment_layout, container, false
        )
        rootView?.editModeContainer?.visibility = View.GONE
        rootView?.viewModeContainer?.visibility = View.VISIBLE
        setListeners()
        fillLayout()


        return rootView
    }

    private fun fillLayout() {
        // TODO: replace the from somewheres :D
        rootView?.titleText?.text = operation?.operationDescription
        rootView?.titleTextValue?.setText(operation?.operationDescription)
        rootView?.assignedToText?.text = operation?.personName
        rootView?.assignedToTextValue?.setText(operation?.personName)
        rootView?.dateText?.text = operation?.startDate.toString()
        rootView?.dateTextValue?.setText(operation?.startDate.toString())

        rootView?.startTimeValue?.setText(operation?.startTime.toString())
        rootView?.endTimeValue?.setText(operation?.endTime.toString())

        rootView?.taskIdentifierText?.text = operation?.workOrderID
        /*
        rootView?.criticalPathText?.text = "crital from somewhere"
        // TODO: fill the autocomplete
        rootView?.positionText?.text = "position from somewhere"
        // TODO: fill the autocomplete
        rootView?.materialsText?.text = "material from somewhere"
        // TODO: fill the autocomplete
        rootView?.otherConstraintsText?.text = "constrains from somewhere"
        // TODO: fill the autocomplete
        rootView?.predecessorsText?.text = "predecessors from somewhere"
        // TODO: fill the autocomplete
        rootView?.sucessorsText?.text = "Successors from somewhere"
        // TODO: fill the autocomplete*/

    }

    private fun setListeners() {
        rootView?.doneBtn?.setOnClickListener { dismiss() }
        rootView?.editBtn?.setOnClickListener {
            rootView?.editModeContainer?.visibility = View.VISIBLE
            rootView?.viewModeContainer?.visibility = View.GONE
            rootView?.tasksApplyBtn?.visibility = View.VISIBLE
            rootView?.taskCancelBtn?.visibility = View.VISIBLE
            rootView?.doneBtn?.visibility = View.GONE
            rootView?.editBtn?.visibility = View.GONE
        }
        rootView?.taskCancelBtn?.setOnClickListener {
            rootView?.editModeContainer?.visibility = View.GONE
            rootView?.viewModeContainer?.visibility = View.VISIBLE
            fillLayout()
            rootView?.editBtn?.visibility = View.VISIBLE
            rootView?.tasksApplyBtn?.visibility = View.GONE
            rootView?.taskCancelBtn?.visibility = View.GONE
            rootView?.doneBtn?.visibility = View.VISIBLE
        }
        rootView?.tasksApplyBtn?.setOnClickListener { updateOperationAndSave() }
        rootView?.assignedToTextValue?.setOnClickListener {
            activity?.supportFragmentManager?.let { it1 ->
                AddWorkersPopUpFragment(getString(R.string.done)){

                }.show(it1,AddWorkersPopUpFragment.ADD_WORKERS_POPUP_TAG)
            }
        }
        rootView?.dateTextValue?.setOnClickListener {
            context?.also { context ->
                val cal = Calendar.getInstance()

                val dateSetListener =
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        cal.set(Calendar.YEAR, year)
                        cal.set(Calendar.MONTH, monthOfYear)
                        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                        val sdf = SimpleDateFormat("EE, dd MMM, yyyy", Locale.getDefault())
                        sdf.format(cal.time).also { rootView?.dateTextValue?.setText(it) }
                    }
                DatePickerDialog(
                    context,
                    dateSetListener,
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }
        rootView?.startTimeValue?.setOnClickListener {
            context?.also { context ->
                showTimePicker(context, rootView?.startTimeValue)
            }

        }

        rootView?.endTimeValue?.setOnClickListener {
            context?.also { context ->
                showTimePicker(context, rootView?.endTimeValue)
            }
        }
    }

    private fun showTimePicker(context: Context, timeEdit: TextView?) {
        val calendar = Calendar.getInstance()
        val mHour = calendar.get(Calendar.HOUR_OF_DAY);
        val mMinute = calendar.get(Calendar.MINUTE);

        val timeListener =
            TimePickerDialog.OnTimeSetListener { timePicker: TimePicker, hour: Int, minute: Int ->
                timeEdit?.setText(String.format("%02d", hour)+ ":" + String.format("%02d",minute))
            }

        val timePickerDialog = TimePickerDialog(
            context,
            timeListener,
            mHour,
            mMinute,
            true
        );
        timePickerDialog.show();
    }

    private fun updateOperationAndSave() {
        // TODO: Implement saving
        dismiss()
    }

    companion object {
        const val TASK_DETAILS_TAG = "com.deloitte.opticrewandroid.app.ui.TaskDetailsPopUpFragment"
    }
}