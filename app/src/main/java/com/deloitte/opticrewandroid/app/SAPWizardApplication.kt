package com.deloitte.opticrewandroid.app

import android.app.Application
import android.content.SharedPreferences
import com.sap.cloud.mobile.foundation.authentication.AppLifecycleCallbackHandler
import com.sap.cloud.mobile.foundation.model.AppConfig
import androidx.preference.PreferenceManager
import com.deloitte.opticrewandroid.service.SAPServiceManager
import com.deloitte.opticrewandroid.repository.RepositoryFactory
import com.sap.cloud.mobile.foundation.mobileservices.MobileService
import com.sap.cloud.mobile.foundation.mobileservices.SDKInitializer
import com.sap.cloud.mobile.foundation.logging.Logging
import com.sap.cloud.mobile.foundation.logging.LogService
import ch.qos.logback.classic.Level


class SAPWizardApplication: Application() {

    /** The [AppConfig] of this application */
    var appConfig: AppConfig? = null
    internal var isApplicationUnlocked = false
    lateinit var preferenceManager: SharedPreferences

    /**
     * Manages and provides access to OData stores providing data for the app.
     */
    internal var sapServiceManager: SAPServiceManager? = null
    /**
     * Application-wide RepositoryFactory
     */
    lateinit var repositoryFactory: RepositoryFactory
        private set

    override fun onCreate() {
        super.onCreate()
        preferenceManager = PreferenceManager.getDefaultSharedPreferences(this)
        registerActivityLifecycleCallbacks(AppLifecycleCallbackHandler.getInstance())
        initServices()
    }


    fun initializeServiceManager(appConfig: AppConfig) {
        sapServiceManager = SAPServiceManager(appConfig)
        repositoryFactory =
            RepositoryFactory(sapServiceManager)
    }

    /**
     * Clears all user-specific data and configuration from the application, essentially resetting it to its initial
     * state.
     *
     * If client code wants to handle the reset logic of a service, here is an example:
     *
     *   SDKInitializer.resetServices { service ->
     *       return@resetServices if( service is PushService ) {
     *           PushService.unregisterPushSync(object: CallbackListener {
     *               override fun onSuccess() {
     *               }
     *
     *               override fun onError(p0: Throwable) {
     *               }
     *           })
     *           true
     *       } else {
     *           false
     *       }
     *   }
     */
    fun resetApplication() {
        preferenceManager.also {
            it.edit().clear().apply()
        }
        appConfig = null
        isApplicationUnlocked = false
        repositoryFactory.reset()
        SDKInitializer.resetServices()
    }

    private fun initServices() {
        val services = mutableListOf<MobileService>()
        Logging.setConfigurationBuilder(Logging.ConfigurationBuilder().initialLevel(Level.WARN).logToConsole(true).build())
        services.add(LogService())


        SDKInitializer.start(this, * services.toTypedArray(), apiKey = "3ffcae6d-e184-4889-8760-f4042b3eb83f")
    }

    companion object {
        const val KEY_LOG_SETTING_PREFERENCE = "key.log.settings.preference"
    }
}