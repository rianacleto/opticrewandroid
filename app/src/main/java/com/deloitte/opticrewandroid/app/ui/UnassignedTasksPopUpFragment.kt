package com.deloitte.opticrewandroid.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.adapters.AddWorkersPopUpAdapter
import com.deloitte.opticrewandroid.adapters.UnassignedTasksAdapter
import com.deloitte.opticrewandroid.data.AddWorkerItem
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.OperationsType
import kotlinx.android.synthetic.main.add_workers_popup_layout.view.*
import kotlinx.android.synthetic.main.unassigned_tasks_popup_layout.view.*

class UnassignedTasksPopUpFragment(
    private val onAutoAssignAction: (List<String>) -> Unit
) : DialogFragment() {

    private var unassignedTasksList = mutableListOf<OperationsType>()
    private lateinit var rootView: View
    private val unassignedTasksAdapter by lazy {
        context?.let { UnassignedTasksAdapter(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.isCancelable = false
        rootView = inflater.inflate(R.layout.unassigned_tasks_popup_layout, container, false)

        rootView.unassign_tasks_recycler.adapter = unassignedTasksAdapter
        val layoutManager = LinearLayoutManager(context)
        rootView.unassign_tasks_recycler.layoutManager = layoutManager

        setListeners()

        return rootView
    }

    private fun setListeners() {
        rootView.unassign_tasks_close.setOnClickListener { dismiss() }
    }

    companion object {
        const val UNASSIGNED_TASKS_POPUP_TAG =
            "com.deloitte.opticrewandroid.app.ui.UnassignedTasksPopUpFragment"
    }
}