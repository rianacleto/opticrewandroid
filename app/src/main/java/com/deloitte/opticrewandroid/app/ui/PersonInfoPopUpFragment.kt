package com.deloitte.opticrewandroid.app.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.deloitte.opticrewandroid.R
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.EmployeesType
import com.sap.cloud.android.odata.cds_zsd_sam_odata_entities.WorkCenterEmployeesType
import kotlinx.android.synthetic.main.person_info_poup_layout.view.*


class PersonInfoPopUpFragment(
    private val person: WorkCenterEmployeesType,
    private val onUnassignAction: (personNum: String) -> Unit
) : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.isCancelable = false
        val rootView = inflater.inflate(R.layout.person_info_poup_layout, container, false)

        setListeners(rootView)
        setLists(rootView)
        fillData(rootView)

        return rootView
    }

    private fun setListeners(rootView: View) {
        rootView.unassign_tasks.setOnClickListener {
            onUnassignAction(person.personNumber)
        }

        rootView.doneButton.setOnClickListener { dismiss() }

        rootView.callContainer.setOnClickListener {
            val dialIntent = Intent(Intent.ACTION_DIAL)
            // TODO: Get number from somewhere...
            dialIntent.data = Uri.parse("tel:" + "123456789")
            startActivity(dialIntent)
        }

        rootView.textContainer.setOnClickListener {
            // TODO: Also get the number here
            val uri = Uri.parse("smsto:123456789")
            startActivity(Intent(Intent.ACTION_SENDTO, uri))
        }

        rootView.emailContainer.setOnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)

            // TODO: Get email from somewhere
            intent.data = Uri.parse("mailto: Example@gmail.com")
            startActivity(intent)
        }
    }

    private fun setLists(rootView: View) {
        // TODO: check where to get the qualifications and medical conditions from
        context?.also {

            val qualifications = mutableListOf<String>(person?.qualification ?: "")
            val qualificationsAdapter: ArrayAdapter<String> = ArrayAdapter(
                it,
                R.layout.person_info_list_item,
                R.id.itemId,
                qualifications
            )


            val medicalVals = mutableListOf<String>("Asthma")
            val medicalAdapter: ArrayAdapter<String> = ArrayAdapter(
                it,
                R.layout.person_info_list_item,
                R.id.itemId,
                medicalVals
            )

            rootView.qualificationsList.adapter = qualificationsAdapter
            rootView.medicalConditionsList.adapter = medicalAdapter
        }

    }

    private fun fillData(rootView: View) {
        rootView.avatar.setImageResource(R.drawable.ic_android_blue)
        rootView.personName.text = person.personName
        rootView.skill.text = person.qualificationLevel
        // TODO: Get next shift
        rootView.nextShift.text = "TODO: Get my next shift"
    }

    companion object{
        const val PERSON_DETAILS_TAG = "com.deloitte.opticrewandroid.app.ui.PersonInfoPopUpFragment"
    }

}