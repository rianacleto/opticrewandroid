package com.deloitte.opticrewandroid.app.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.deloitte.opticrewandroid.R
import com.deloitte.opticrewandroid.adapters.AddWorkersPopUpAdapter
import com.deloitte.opticrewandroid.data.AddWorkerItem
import kotlinx.android.synthetic.main.add_workers_popup_layout.view.*

class AddWorkersPopUpFragment(
    private val applyText: String,
    private val onApplyAction: (List<String>) -> Unit
) : DialogFragment() {

    private var addWorkersList = mutableListOf<AddWorkerItem>()
    private lateinit var rootView: View
    private val addWorkersPopUpAdapter by lazy {
        context?.let { AddWorkersPopUpAdapter(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        this.isCancelable = false
        rootView = inflater.inflate(R.layout.add_workers_popup_layout, container, false)

        rootView.add_workers_people_recycler.adapter = addWorkersPopUpAdapter
        val layoutManager = LinearLayoutManager(context)
        rootView.add_workers_people_recycler.layoutManager = layoutManager

        setListeners()

        return rootView
    }

    private fun setListeners() {
        rootView.add_workers_search_view.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                addWorkersPopUpAdapter?.setData(addWorkersList.filter { it.workerName == query })
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                addWorkersPopUpAdapter?.setData(addWorkersList.filter { it.workerName == query })
                return true
            }
        })
        rootView.add_workers_back_arrow.setOnClickListener { dismiss() }
        rootView.add_workers_cancel.setOnClickListener { dismiss() }
        rootView.add_workers_add.setText(applyText)
        rootView.add_workers_add.setOnClickListener {
            onApplyAction(getPersonNumbersToAdd())
        }
    }

    private fun getPersonNumbersToAdd():List<String>{
        val list = mutableListOf<String>()
        for(item in addWorkersList){
            list.add(item.workerNumber)
        }
        return list
    }

    companion object {
        const val ADD_WORKERS_POPUP_TAG =
            "com.deloitte.opticrewandroid.app.ui.AddWorkersPopUpFragment"
    }
}