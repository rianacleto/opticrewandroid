package com.deloitte.opticrewandroid.configurations

object Constants {
    const val HEADER_AND_EMPTY_HEIGH = 50
    const val DATA_AND_PERSON_ROW_HIGH = 100
    const val PERSON_ROW_WIDTH = 300
    const val MIN_TO_DPS_RATIO = 2
    const val HEADER_WIDTH = 120
}